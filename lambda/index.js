const Alexa = require('ask-sdk-core');
const alexa_helper = require('./helpers/alexa_helper');
const modes_helper = require('./helpers/modes_helper')
const persistenceAdapter = require('ask-sdk-s3-persistence-adapter');

const report_access_controller = require('./controllers/report_access_controller');
const help_command_controller = require('./controllers/help_command_controller');
const report_selection_controller = require('./controllers/report_selection_controller');
const errors_controller = require('./controllers/errors_controller');
const profile_controller = require('./controllers/profile_controller');
const interceptors_controller = require('./controllers/interceptors_controller');
const launch_controller = require('./controllers/launch_controller');

const BadLaunchRequestHandler = {
    async canHandle(handlerInput) {
        return launch_controller.check_bad_launch_request(handlerInput);
    },
    handle(handlerInput) {
        return launch_controller.handle_bad_launch_request(handlerInput);
    }
};

const LaunchRequestHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'LaunchRequest';
    },
    async handle(handlerInput) {
        return launch_controller.handle_launch_request(handlerInput);
    }
};

const FullReportIntentHandler = {
    canHandle(handlerInput) {
      return alexa_helper.is_one_of_these_intents(handlerInput, 'FullReportIntent');
    },
    async handle(handlerInput) {
        return report_access_controller.handle_full_report_access(handlerInput);
    }
};

const HelpIntentHandler = {
    canHandle(handlerInput) {
        return alexa_helper.is_one_of_these_intents(handlerInput, ['AMAZON.HelpIntent', 'SpecificHelpIntent']);
    },
    handle(handlerInput) {
        return help_command_controller.handle_help_intent(handlerInput);
    }
};

const SelectReportByDateIntentHandler = {
    canHandle(handlerInput) {
      return alexa_helper.is_one_of_these_intents(handlerInput, 'SelectReportByDateIntent');
    },
    async handle(handlerInput) {
        return report_selection_controller.handle_select_report_by_date(handlerInput);
    }
};

const IncrementalReportIntentHandler = {
    canHandle(handlerInput) {
      return modes_helper.is_one_of_mode_related_intents(handlerInput, 'INCREMENTAL_ACCESS');
    },
    async handle(handlerInput) {
        return report_access_controller.handle_incremental_report_access(handlerInput);
    }
};

const OnDemandReportIntentHandler = {
    canHandle(handlerInput) {
        return modes_helper.is_one_of_mode_related_intents(handlerInput, 'ON_DEMAND_ACCESS');
    },
    async handle(handlerInput) {
        return report_access_controller.handle_on_demand_report_access(handlerInput);
    }
}

const SetUserApiIdIntentHandler = {
    canHandle(handlerInput) {
      return alexa_helper.is_one_of_these_intents(handlerInput, 'SetUserApiIdIntent');
    },
    async handle(handlerInput) {
      return profile_controller.handle_set_user_api_id_in_persistence(handlerInput);
    }
};

const GetUserApiIdIntentHandler = {
    canHandle(handlerInput) {
      return alexa_helper.is_one_of_these_intents(handlerInput, 'GetUserApiIdIntent');
    },
    async handle(handlerInput) {
      return profile_controller.handle_get_user_api_id_from_persistence(handlerInput);
    }
};

const ClearProfileIntentHandler = {
    canHandle(handlerInput) {
        return alexa_helper.is_one_of_these_intents(handlerInput, 'ClearProfileIntent');
      },
      async handle(handlerInput) {
        return profile_controller.handle_clear_profile_in_persistence(handlerInput);
      }
}

const MergePersistenceAttributesInSessionRequestInterceptor = {
    async process(handlerInput) {
        await interceptors_controller.handle_merge_persistence_attributes_to_session(handlerInput);
    }

}

const PutCurrentAccessModeInSessionRequestInterceptor = {
    async process(handlerInput) {
        if (handlerInput.requestEnvelope.request.type === 'IntentRequest'){
            await interceptors_controller.handle_put_current_access_mode_in_session(handlerInput);
        }
            
        return;
    }
}

const LogRequestInterceptor = {
    async process(handlerInput) {
        await interceptors_controller.handle_log_request_data(handlerInput);
            
        return;
    }
}

const LogResponseInterceptor = {
    async process(handlerInput) {
        await interceptors_controller.handle_log_response_data(handlerInput);
            
        return;
    }
}

// const PreviousIntentSavingResponseInterceptor = {
//     process(handlerInput) {
//         if (handlerInput.requestEnvelope.request.type === 'IntentRequest'){
//             interceptors_controller.save_previous_intent_in_session(handlerInput);
//         }
            
//         return;
//     }
// }

// The intent reflector is used for interaction model testing and debugging.
// It will simply repeat the intent the user said. You can create custom handlers
// for your intents by defining them above, then also adding them to the request
// handler chain below.
const IntentReflectorHandler = {
    canHandle(handlerInput) {
        return Alexa.getRequestType(handlerInput.requestEnvelope) === 'IntentRequest';
    },
    handle(handlerInput) {
        const intentName = Alexa.getIntentName(handlerInput.requestEnvelope);
        const speakOutput = `Acabas de lanzarme un comando de tipo ${intentName}`;

        return handlerInput.responseBuilder
            .speak(speakOutput)
            //.reprompt('add a reprompt if you want to keep the session open for the user to respond')
            .getResponse();
    }
};

// Generic error handling to capture any syntax or routing errors. If you receive an error
// stating the request handler chain is not found, you have not implemented a handler for
// the intent being invoked or included it in the skill builder below.
const ErrorHandler = {
    canHandle(){
        return true;
    },
    async handle(handlerInput, error) {
        console.log('HANDLER - ' + error.type)
        return errors_controller.handle_error_event(handlerInput, error);
    }
};

// The SkillBuilder acts as the entry point for your skill, routing all request and response
// payloads to the handlers above. Make sure any new handlers or interceptors you've
// defined are included below. The order matters - they're processed top to bottom.
exports.handler = Alexa.SkillBuilders.custom()
    .addRequestHandlers(
        BadLaunchRequestHandler,
        LaunchRequestHandler,
        FullReportIntentHandler,
        IncrementalReportIntentHandler,
        IncrementalReportIntentHandler,
        OnDemandReportIntentHandler,
        HelpIntentHandler,
        SelectReportByDateIntentHandler,
        SetUserApiIdIntentHandler,
        GetUserApiIdIntentHandler,
        ClearProfileIntentHandler,
        IntentReflectorHandler, // make sure IntentReflectorHandler is last so it doesn't override your custom intent handlers
    )
    .addRequestInterceptors(
        LogRequestInterceptor,
        MergePersistenceAttributesInSessionRequestInterceptor,
        PutCurrentAccessModeInSessionRequestInterceptor,
    )
    .addResponseInterceptors(
        LogResponseInterceptor
    )
    .addErrorHandlers(
        ErrorHandler,
    )
    // .addResponseInterceptors(
    //     PreviousIntentSavingResponseInterceptor
    // )
    .withPersistenceAdapter(
        new persistenceAdapter.S3PersistenceAdapter({bucketName:process.env.S3_PERSISTENCE_BUCKET})
    )
    .lambda();
