const s3persistence_manager = require('../managers/s3persistence_manager');
const session_manager = require('../managers/session_manager')
const error_helper = require('../helpers/error_handling_helper')


/**
 * A method to put profile's user api id in s3 persistence layer.
 * 
 * @param {ask_sdk.handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling a SetUserApiIdIntent request.
 * @param {string} amazon_user_id - A string with the amazon user id from alexa request.
 * @param {number|string} api_user_id - A number or string with the user api id to set in persistence profile.
 * 
 * @return {object} An object with the account updated profile attributes.
 * 
 */
const set_user_api_id_in_persistence = (handlerInput, api_user_id) => {

    let s3Attributes = {};
    s3Attributes["api_user"] = api_user_id;

    return new Promise((resolve, reject) => {
        
        s3persistence_manager.update_persistence_attributes(handlerInput, s3Attributes)
        .then( updated_attributes => {
            resolve(updated_attributes);
        })
        .catch(reject);
    });
    
};

/**
 * A method to put profile's user api id in s3 persistence layer.
 * 
 * @param {ask_sdk.handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling a SetUserApiIdIntent request.
 * 
 * @return {string} A string with the user api id from s3 persistence layer.
 * 
 */
const get_user_api_id_from_persistence = (handlerInput) => {

    return new Promise((resolve, reject) => {
        
        s3persistence_manager.get_persistence_attribute(handlerInput, 'api_user')
        .then( api_user_id => {
            resolve(api_user_id);
        })
        .catch(reject);
    });
    
};

/**
 * A method to reset all attributes in s3 persistence layer.
 * 
 * @param {ask_sdk.handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling a SetUserApiIdIntent request.
 * 
 */
const clear_profile_in_persistence = (handlerInput) => {

    var s3Attributes = {};

    return new Promise((resolve, reject) => {
        
        s3persistence_manager.set_persistence_attributes(handlerInput, s3Attributes)
        .then( updated_attributes => {
            resolve(updated_attributes);
        })
        .catch(reject);
    });
    
};


/**
 * A method to put profile's user api id in session.
 * 
 * @param {ask_sdk.handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling an alexa request.
 * @param {number|string} api_user_id - A number or string with the user api id to set in session.
 * 
 * @return {ask_sdk.handlerInput} The same handlerInput object from ask-sdk module
 *  aupdated with the new attribute in session.
 * 
 */
const put_user_api_id_in_session = (handlerInput, api_user_id) => {

    let attr_name = 'api_user';
    let new_attributes = {};
    new_attributes[attr_name] = api_user_id;
    let updated_handlerInput = session_manager.update_session_attributes( handlerInput, new_attributes );

    return updated_handlerInput;
    
};

/**
 * A method to get profile's user api id from session.
 * 
 * @param {ask_sdk.handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling an alexa request.
 * 
 * @return {number|string} A number or string with the api user id stored in request session.
 * 
 */
const get_user_api_id_from_session = (handlerInput) => {
    
    try {
        var attr_name = 'api_user';
        var api_user_id = session_manager.get_session_attribute( handlerInput, attr_name );
    
        return api_user_id;
    }
    
    catch(err) {
        throw err;
    }
    
};

module.exports = { set_user_api_id_in_persistence,
                   get_user_api_id_from_persistence,
                   clear_profile_in_persistence,
                   put_user_api_id_in_session,
                   get_user_api_id_from_session }