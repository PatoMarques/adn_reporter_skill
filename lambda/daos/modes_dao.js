const session_manager = require('../managers/session_manager');

/**
 * A method to put the current access mode in session.
 * 
 * @param {ask_sdk.handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling an alexa request.
 * @param {string} current_access_mode - A string with the current access mode to set in session.
 * 
 * @return {ask_sdk.handlerInput} The same handlerInput object from ask-sdk module
 *  aupdated with the new attribute in session.
 * 
 */
const put_current_access_mode_in_session = (handlerInput, current_access_mode) => {

    let attr_name = 'current_access_mode';
    let new_attributes = {};
    new_attributes[attr_name] = current_access_mode;
    let updated_handlerInput = session_manager.update_session_attributes( handlerInput, new_attributes );

    return updated_handlerInput;
    
};

/**
 * A method to get the current access mode from session.
 * 
 * @param {ask_sdk.handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling an alexa request.
 * 
 * @return {number|string} A string with the current access mode stored in request session.
 * 
 */
const get_current_access_mode_from_session = (handlerInput) => {
    
    try {
        let attr_name = 'current_access_mode';
        let current_access_mode = session_manager.get_session_attribute( handlerInput, attr_name );
    
        return current_access_mode;
    }
    
    catch(err) {
        throw err;
    }
    
};

module.exports = { put_current_access_mode_in_session, get_current_access_mode_from_session }