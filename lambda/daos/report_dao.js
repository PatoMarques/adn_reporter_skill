const Report = require('../dtos/report_dto');
const { build_uri_from_date } = require ('../helpers/api_helper');
const { get_json_response_from_api } = require('../managers/api_manager');
const { build_multiple_report_dtos_from_api_response, transform_report_dto_to_json, build_report_dto_from_json } = require('../helpers/report_helper');
const { get_session_attribute, update_session_attributes } = require('../managers/session_manager');
const error_helper = require('../helpers/error_handling_helper');


/**
 * A method to retrieve a collection of reports from the api given the date in prepared format.
 * 
 * @param {object} prepared_date - An object with the date parameters to compose the url of the requested report.
 * @param {number} prepared_date.year - A number indicating the year of the reports to retrieve.
 * @param {number} prepared_date.month - A number indicating the month of the reports to retrieve, starting from 1 for January.
 * @param {number} prepared_date.month_period - A number indicating the period of the month of the reports to retrieve,
 *  being 1 the first half, and 2 the second half.
 * 
 * @return {Report[]} A collection of Report objects of one date in particular.
 * 
 */
const get_reports_from_api = (prepared_date) => {

    url = build_uri_from_date(prepared_date);

    return new Promise((resolve, reject) => {
        build_uri_from_date(prepared_date)
        .then( url => get_json_response_from_api(url))
        .then(reports => {
            console.log('reports taken successfully from api.');
            reports = build_multiple_report_dtos_from_api_response(reports, prepared_date);
            resolve(reports);
        })
        .catch(err => {
            reject(error_helper.throw_new_custom_error_if_not_handled(err, 'build_report_object', true));
        });
    });
    
};

/**
 * A method to get the requested report from alexa session object.
 * 
 * @param {ask_sdk.handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling a request.
 * 
 * @return {Report} A Report object built from JSON formatted report in session.
 * 
 */
const get_requested_report_from_session = (handlerInput) => {
    try {
        attr_name = 'requested_report';
        json_report = get_session_attribute(handlerInput, attr_name);
        return build_report_dto_from_json(json_report);
    }
    catch(err) {
        error_helper.throw_new_custom_error_if_error_type(err, 'session_attr_not_exists', 'session_requested_report_not_exists');
        error_helper.throw_new_custom_error_if_not_handled(err, 'session_requested_report_invalid_format');
    }
};

/**
 * A method to put the requested report into alexa session object.
 * 
 * @param {ask_sdk.handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling a request.
 * 
 * @param {Report} report - A Report object to put inside Alexa session object.
 * 
 * @return {ask_sdk.handlerInput} the updated handlerInput with the JSON formatted Report object in its session.
 * 
 */
const put_requested_report_in_session = (handlerInput, report) => {
    attr_name = 'requested_report';
    new_attributes = {};
    new_attributes[attr_name] = transform_report_dto_to_json(report);
    updated_handlerInput = update_session_attributes( handlerInput, new_attributes );

    return updated_handlerInput;
};

/**
 * A method to get the current report increment index into alexa session object.
 * 
 * @param {ask_sdk.handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling a request.
 * 
 * @return {number} An integer with the index of the first item of the current increment from Alexa session object.
 * 
 */
const get_requested_increment_index_from_session = (handlerInput) => {
    try {
        attr_name = 'increment_index';
        increment_index = get_session_attribute(handlerInput, attr_name);
        return increment_index;
    }
    catch(err) {
        error_helper.throw_new_custom_error_if_error_type(err, 'session_attr_not_exists', 'session_requested_report_not_exists');
        error_helper.throw_new_custom_error_if_not_handled(err, 'session_requested_report_invalid_format');
    }
};

/**
 * A method to put the next report increment index into alexa session object.
 * 
 * @param {ask_sdk.handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling a request.
 * 
 * @param {number} index - An integer with the index of the last item of the current increment to put inside Alexa session object.
 * 
 * @return {ask_sdk.handlerInput} the updated handlerInput with the JSON formatted Report object in its session.
 * 
 */
const put_requested_increment_index_in_session = (handlerInput, index) => {
    attr_name = 'increment_index';
    new_attributes = {};
    new_attributes[attr_name] = index;
    updated_handlerInput = update_session_attributes( handlerInput, new_attributes );
    return updated_handlerInput;
};

/**
 * A method to get the already listened comments into alexa session object.
 * 
 * @param {ask_sdk.handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling a request.
 * 
 * @return {number[]} An array of integers with the indexes of the comments that are already listened by user in this session,
 *  from Alexa session object.
 * 
 */
const get_listened_comments_from_session = (handlerInput) => {
    try {
        attr_name = 'listened_comments';
        listened_comments = get_session_attribute(handlerInput, attr_name);
        return listened_comments;
    }
    catch(err) {
        error_helper.throw_new_custom_error_if_error_type(err, 'session_attr_not_exists', 'session_listened_comments_not_exists');
    }
};

/**
 * A method to put the listened comments index into alexa session object.
 * 
 * @param {ask_sdk.handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling a request.
 * 
 * @param {number[]} listened_comments - An array of indexes of the listened comments by user in this session,
 *  to put inside Alexa session object.
 * 
 * @return {ask_sdk.handlerInput} the updated handlerInput with the listened comments in its session.
 * 
 */
const put_listened_comments_in_session = (handlerInput, listened_comments) => {
    attr_name = 'listened_comments';
    new_attributes = {};
    new_attributes[attr_name] = listened_comments;
    updated_handlerInput = update_session_attributes( handlerInput, new_attributes );
    return updated_handlerInput;
};

/**
 * A method to get the requested comments in this intent, into alexa session object.
 * 
 * @param {ask_sdk.handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling a request.
 * 
 * @return {number[]} An array of integers with the indexes of the comments that are going to be listened by user in this intent response,
 *  from Alexa session object.
 * 
 */
const get_requested_items_from_session = (handlerInput) => {
    try {
        attr_name = 'requested_items';
        requested_items = get_session_attribute(handlerInput, attr_name);
        return requested_items;
    }
    catch(err) {
        error_helper.throw_new_custom_error_if_error_type(err, 'session_attr_not_exists', 'session_listened_comments_not_exists');
    }
};

/**
 * A method to put the requested_items into alexa session object.
 * 
 * @param {ask_sdk.handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling a request.
 * 
 * @param {number[]} requested_items - An array of indexes of the requested items by user in this session,
 *  to put inside Alexa session object.
 * 
 * @return {ask_sdk.handlerInput} the updated handlerInput with the requested comments in its session.
 * 
 */
const put_requested_items_in_session = (handlerInput, requested_items) => {
    attr_name = 'requested_items';
    new_attributes = {};
    new_attributes[attr_name] = requested_items;
    updated_handlerInput = update_session_attributes( handlerInput, new_attributes );
    return updated_handlerInput;
};


module.exports = {  
    get_reports_from_api, 
    get_requested_report_from_session, 
    put_requested_report_in_session,
    get_requested_increment_index_from_session,
    put_requested_increment_index_in_session,
    get_listened_comments_from_session,
    put_listened_comments_in_session,
    get_requested_items_from_session,
    put_requested_items_in_session
}