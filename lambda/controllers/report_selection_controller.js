const { preprocess_report_date } = require('../helpers/date_helper');
const report_helper = require('../helpers/report_helper');
const report_interface = require('../interfaces/report_interface');
const { build_json_response } = require('../helpers/alexa_utils');
const { put_requested_report_in_session, 
        get_requested_report_from_session } = require('../daos/report_dao');
const { build_select_report_by_date_speech_text } = require('../dialog_builders/select_report_by_date_dialog_builder');
const profile_dao = require('../daos/profile_dao');


/**
 * A method to handle and render the response for the SelectByDateIntent.
 * 
 * @param {ask_sdk.handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling a SelectByDateIntent request.
 * 
 * 
 * @return {ask_sdk.Response} An object representing the SelectByDateIntent response that's going to be sent back to alexa service.
 * 
 */
const handle_select_report_by_date = async (handlerInput) => {

    try {
        var prepared_date = preprocess_report_date(handlerInput.requestEnvelope.request.intent.slots);
        var api_user_id = profile_dao.get_user_api_id_from_session(handlerInput);
    }catch(err){throw err}
  
    return report_interface.get_reports_from_api(prepared_date)
    .then(reports => {
        try{
            var report = report_helper.get_user_specific_report_from_api(reports, api_user_id);
        } catch(err) {throw err};

        handlerInput = put_requested_report_in_session(handlerInput, report);
        report = get_requested_report_from_session(handlerInput);

        speechText = build_select_report_by_date_speech_text(prepared_date, report.items.length);

        return build_json_response(handlerInput, {speechText: speechText, should_end_session: false});
    })
    .catch((error) => {
        throw error;
    })
}


module.exports = { handle_select_report_by_date }