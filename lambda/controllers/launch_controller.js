const alexa_helper = require('../helpers/alexa_helper');
const error_helper = require('../helpers/error_handling_helper');
const launch_request_dialog_builder = require('../dialog_builders/launch_request_dialog_builder')

/**
 * A method to build the response for a fullreport intent
 *  request from the stored report in session.
 *
 * @param {handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling a request. 
 * @return {string} A JSON response with all the data to send back to Alexa service.
 * 
 */
const check_bad_launch_request = (handlerInput) => {

    const attributes = handlerInput.attributesManager.getSessionAttributes();
    const is_user_ko = !attributes.hasOwnProperty('api_user');
    const is_not_launch = (handlerInput.requestEnvelope.request.type === 'LaunchRequest') == false;
    const is_not_set_profile_intent = !alexa_helper.is_one_of_these_intents(handlerInput, ['SetUserApiIdIntent'])

    console.log(`BAD_LAUNCH - ` + JSON.stringify(attributes));
    console.log(`BAD_LAUNCH - USER_KO: ${is_user_ko}  NOT_LAUNCH: ${is_not_launch}`);

    return is_not_set_profile_intent && is_not_launch && is_user_ko;
}

/**
 * A method to build the response for a fullreport intent
 *  request from the stored report in session.
 *
 * @param {handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling a request. 
 * @return {string} A JSON response with all the data to send back to Alexa service.
 * 
 */
const handle_bad_launch_request = (handlerInput) => {
    const attributes = handlerInput.attributesManager.getSessionAttributes();
    console.log('BAD LAUNCH HANDLER - ' + JSON.stringify(attributes));
    error_helper.throw_new_custom_error('s3_persistence_profile_not_exists');

}

/**
 * A method to build the response for a launch request.
 *
 * @param {handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling a request. 
 * @return {string} A JSON response with all the data to send back to Alexa service.
 * 
 */
const handle_launch_request = (handlerInput) => {

    const attributes = handlerInput.attributesManager.getSessionAttributes();
    const is_user_ko = !attributes.hasOwnProperty('api_user');

    var speechOutput = launch_request_dialog_builder.build_launch_speech_text(!is_user_ko);

    return alexa_helper.build_json_response(handlerInput, {speechText: speechOutput, should_end_session: false});
}

module.exports = {check_bad_launch_request, handle_bad_launch_request, handle_launch_request}