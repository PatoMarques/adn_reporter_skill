const full_report_dialog_builder = require('../dialog_builders/full_report_dialog_builder');
const incremental_report_dialog_builder = require('../dialog_builders/incremental_report_dialog_builder');
const on_demand_report_dialog_builder = require('../dialog_builders/on_demand_report_dialog_builder');
const alexa_helper = require('../helpers/alexa_helper');
const report_dao = require('../daos/report_dao');
const report_helper = require('../helpers/report_helper');


/**
 * A method to build the response for a fullreport intent
 *  request from the stored report in session.
 *
 * @param {handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling a request. 
 * @return {string} A JSON response with all the data to send back to Alexa service.
 * 
 */
const handle_full_report_access = (handlerInput) => {

        let requested_report = report_dao.get_requested_report_from_session(handlerInput);
        let speechText = full_report_dialog_builder.build_full_report_speech_text(requested_report);
        
        return alexa_helper.build_json_response(handlerInput, {speechText: speechText, should_end_session: true});

}

/**
 * A method to build the response for a incremental report access intent
 *  request from the stored report in session.
 *
 * @param {handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling a request. 
 * @return {string} A JSON response with all the data to send back to Alexa service.
 * 
 */
const handle_incremental_report_access = (handlerInput) => {

        var params = __parameterize_incremental_controller_by_intent_name(handlerInput);

        var requested_report = report_dao.get_requested_report_from_session(handlerInput);

        var increment_result = report_helper.get_report_increment(requested_report, params);

        handlerInput = report_dao.put_requested_increment_index_in_session(handlerInput, increment_result.updated_index);

        var speechText = incremental_report_dialog_builder.build_incremental_report_speech_text(increment_result);

        
        
        return alexa_helper.build_json_response(handlerInput, 
                                                {speechText: speechText, should_end_session: increment_result.is_last_increment});

}

/**
 * A method to build the response for a on demand report access intent
 *  request from the stored report in session.
 *
 * @param {handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling a request. 
 * @return {string} A JSON response with all the data to send back to Alexa service.
 * 
 */
const handle_on_demand_report_access = (handlerInput) => {
 
        var params = __parameterize_on_demand_controller_by_intent_name(handlerInput);

        var requested_report = report_dao.get_requested_report_from_session(handlerInput);

        var on_demand_result = report_helper.get_on_demand_data(requested_report, params);

        handlerInput = report_dao.put_listened_comments_in_session(handlerInput, on_demand_result.listened_comments);
        handlerInput = report_dao.put_requested_items_in_session(handlerInput, on_demand_result.requested_items);

        var speechText = on_demand_report_dialog_builder.build_on_demand_report_speech_text(on_demand_result);
        
        return alexa_helper.build_json_response(handlerInput, 
                                                {speechText: speechText, should_end_session: on_demand_result.is_last_on_demand});

}

module.exports = { handle_full_report_access,
                   handle_incremental_report_access,
                   handle_on_demand_report_access };


/**
 * A method to parameterize the controller of incremental report based on intent name.
 *
 * @param {handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling a request. 
 * @return {object} An object with initial params based on intent name.
 * 
 */
const __parameterize_incremental_controller_by_intent_name = (handlerInput) => {

        var params = {}
        params['index'] = alexa_helper.is_one_of_these_intents(handlerInput, 'IncrementalReportIntent') ? 0 
                            : report_dao.get_requested_increment_index_from_session(handlerInput);

        switch(alexa_helper.get_intent_type(handlerInput)){
                case('AMAZON.PreviousIntent'):
                        params['previous'] = true;
                        break;
                case('RepeatCommentsIntent'):
                        params['repeat'] = true;
                        break;
                case('RestOfCommentsIntent'):
                        params['rest_of_increment'] = true;
                        break;
        }
        
                
        return params;
        
}

/**
 * A method to parameterize the controller of on demand report based on intent name.
 *
 * @param {handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling a request. 
 * @return {object} An object with initial params based on intent name.
 * 
 */
const __parameterize_on_demand_controller_by_intent_name = (handlerInput) => {

        var params = {};

        params['listened_comments'] = alexa_helper.is_one_of_these_intents(handlerInput, 'OnDemandReportIntent') ? [] 
                            : report_dao.get_listened_comments_from_session(handlerInput);

        switch(alexa_helper.get_intent_type(handlerInput)){
                case('OnDemandByTypeIntent'):
                        params['requested_type'] = alexa_helper.get_slot_id_from_handler_input(handlerInput, 'item_type');
                        break;
                case('OnDemandByTypeOneIntent'):
                        params['requested_type'] = alexa_helper.get_slot_id_from_handler_input(handlerInput, 'item_type');
                        params['single_comment'] = true;
                        break;
                case('RepeatCommentsIntent'):
                        params['requested_items'] = report_dao.get_requested_items_from_session(handlerInput);
                        params['repeat'] = true;
                        break;
                case('RestOfCommentsIntent'):
                        params['rest'] = true;
        }

        return params;
        
}