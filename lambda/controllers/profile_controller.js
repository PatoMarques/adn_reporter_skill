const alexa_helper = require('../helpers/alexa_helper');
const profile_dao = require('../daos/profile_dao');
const profile_interface = require('../interfaces/profile_interface');
const profile_dialog_builder = require('../dialog_builders/profile_dialog_builder');

/**
 * A method to build the response for a setUserApiId intent
 *  request to the persistence layer.
 *
 * @param {ask_sdk.handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling a SetUserApiIdIntent request.
 * 
 * @return {string} A JSON response with all the data to send back to Alexa service.
 * 
 */
const handle_set_user_api_id_in_persistence = async (handlerInput) => {

    const user_api_id = alexa_helper.get_slot_value_from_handler_input(handlerInput, 'api_user_id');
    
    return profile_interface.set_user_api_id_in_persistence(handlerInput, user_api_id)
           .then((updated_attributes) => {
               profile_dao.put_user_api_id_in_session(handlerInput, user_api_id)
               let speechOutput = profile_dialog_builder.build_set_user_api_id_speech_text(updated_attributes['api_user']);
               return alexa_helper.build_json_response(handlerInput, {speechText: speechOutput, should_end_session: false});
           })
           .catch((error) => {
               throw error;
            })

}

/**
 * A method to build the response for a getUserApiId intent
 *  request from the persistence layer.
 *
 * @param {ask_sdk.handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling a GetUserApiIdIntent request.
 * 
 * @return {string} A JSON response with all the data to send back to Alexa service.
 * 
 */
const handle_get_user_api_id_from_persistence = async (handlerInput) => {

     const userId = alexa_helper.get_amazon_user_id(handlerInput);
     let speechOutput = '';

     return profile_interface.get_user_api_id_from_persistence(handlerInput, userId)
            .then((api_user_id) => {

                 if(api_user_id)
                    speechOutput = profile_dialog_builder.build_get_user_api_id_speech_text(api_user_id);
                 else
                    speechOutput = `Hola, veo que no tienes configurado ningún id de usuario en esta cuenta, para poder acceder a los informes de ADN Mobile, necesitas configurarlo.`;

               return alexa_helper.build_json_response(handlerInput, {speechText: speechOutput, should_end_session: true});
            })
            .catch((error) => {
                 throw error;
             })
 
 }

 /**
 * A method to build the response for a getUserApiId intent
 *  request from the persistence layer.
 *
 * @param {ask_sdk.handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling a GetUserApiIdIntent request.
 * 
 * @return {string} A JSON response with all the data to send back to Alexa service.
 * 
 */
const handle_clear_profile_in_persistence = async (handlerInput) => {

    let speechOutput = '';

    return profile_interface.clear_profile_in_persistence(handlerInput)
           .then(() => {
                speechOutput = profile_dialog_builder.build_clear_profile_speech_text();

                return alexa_helper.build_json_response(handlerInput, {speechText: speechOutput, should_end_session: false});
           })
           .catch((error) => {
                throw error;
            })

}

module.exports = { handle_set_user_api_id_in_persistence,
                   handle_get_user_api_id_from_persistence,
                   handle_clear_profile_in_persistence };