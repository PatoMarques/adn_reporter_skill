const alexa_helper = require('../helpers/alexa_helper');
const error_handling_dialog_builder = require('../dialog_builders/error_handling_dialog_builder');


/**
 * A method to handle and render the response when an error is elevated to one of alexa's handlers in index.
 * 
 * @param {ask_sdk.handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling a SelectByDateIntent request.
 * 
 * @param {Error} error - An object of type Error to process and generate the right speech.
 * 
 * @return {ask_sdk.Response} An object representing the Error response that's going to be
 *  sent back to alexa service when an error happens.
 * 
 */
const handle_error_event = async (handlerInput, error) => {

    console.log('ERRORS - CONTROLLER - ' + error)
    let speechText = error_handling_dialog_builder.build_specific_error_speech_text(error);
    console.log('ERROR CONTROLLER - ' + speechText)

    return alexa_helper.build_json_response(handlerInput, {speechText: speechText, should_end_session: true});

}

module.exports = {handle_error_event};