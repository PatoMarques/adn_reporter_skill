const profile_dao = require('../daos/profile_dao');
const profile_interface = require('../interfaces/profile_interface');
const alexa_helper = require('../helpers/alexa_helper');
const error_helper = require('../helpers/error_handling_helper');
const modes_helper = require('../helpers/modes_helper');
const modes_dao = require('../daos/modes_dao');


// const save_previous_intent_in_session = (handlerInput) => {

//     let current_intent = handlerInput.requestEnvelope.request.intent.name;
    
//     session_dao.put_previous_intent_in_session(handlerInput, current_intent);

// }

/**
 * A method to handle every access mode starter intent requests and save the access mode into session for convenience.
 *
 * @param {ask_sdk.handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling an alexa request.
 * 
 */
const handle_put_current_access_mode_in_session = async (handlerInput) => {

    var access_mode = 'NO_MODE'
    
    var intent_name = alexa_helper.get_intent_type(handlerInput);
    if(alexa_helper.is_one_of_these_intents(handlerInput, modes_helper.get_mode_starter_intents())){
        access_mode = modes_helper.get_access_mode_by_starter_intent_name(intent_name);
    } else if (alexa_helper.is_one_of_these_intents(handlerInput, modes_helper.get_report_access_modes_intents())){
        access_mode = modes_dao.get_current_access_mode_from_session(handlerInput, intent_name);
    }

    try{
        modes_dao.put_current_access_mode_in_session(handlerInput, access_mode);
    } catch(error) {
        throw error;
    }

}

/**
 * A method to handle every access mode starter intent requests and save the access mode into session for convenience.
 *
 * @param {ask_sdk.handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling an alexa request.
 * 
 */
const handle_log_request_data = async (handlerInput) => {

    console.log('REQUEST' + JSON.stringify(handlerInput.requestEnvelope))

}

/**
 * A method to handle every access mode starter intent requests and save the access mode into session for convenience.
 *
 * @param {ask_sdk.handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling an alexa request.
 * 
 */
const handle_log_response_data = async (handlerInput) => {

    console.log('RESPONSE - ' + JSON.stringify(handlerInput.responseBuilder.getResponse()))
    console.log('SESSION ATTRIBUTES - ' + JSON.stringify(handlerInput.attributesManager.getSessionAttributes()))

}

/**
 * A method to handle every access mode starter intent requests and save the access mode into session for convenience.
 *
 * @param {ask_sdk.handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling an alexa request.
 * 
 */
const handle_merge_persistence_attributes_to_session = async (handlerInput) => {

    if (handlerInput.requestEnvelope.session.new){
        var sessionAttributes = handlerInput.attributesManager.getSessionAttributes() || {};

        var persistentAttributes = await handlerInput.attributesManager.getPersistentAttributes();
        console.log('PERSISTENCE ATTRIBUTES - ' + JSON.stringify(persistentAttributes))
        sessionAttributes = Object.assign(sessionAttributes, persistentAttributes);
        console.log('AFTER MERGE - ' + JSON.stringify(sessionAttributes))
        handlerInput.attributesManager.setSessionAttributes(sessionAttributes);
        console.log('AFTER SET MERGE - ' + JSON.stringify(handlerInput.attributesManager.getSessionAttributes()))
    }

    return Promise.resolve();
}

module.exports = { handle_put_current_access_mode_in_session, 
                   handle_log_request_data, handle_log_response_data,
                   handle_merge_persistence_attributes_to_session };