const help_command_helper = require('../helpers/help_command_helper');
const alexa_utils = require('../helpers/alexa_utils');
const help_command_dialog_builder = require('../dialog_builders/help_command_dialog_builder');


/**
 * A method to build the response for the help command intent
 *  request.
 *
 * @param {handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling a request. 
 * @return {string} A JSON response with all the data to send back to Alexa service.
 * 
 */
const handle_help_intent = (handlerInput) => {

    let help_topic = get_help_topic(handlerInput);
    
    let speechText = help_command_dialog_builder.build_help_topic_speech_text(help_topic);

    return alexa_utils.build_json_response(handlerInput, {speechText: speechText, should_end_session: false});

}

/**
 * A private method to pick up the help topic using help_topic slot from handlerInput if exists.
 *
 * @param {handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling a request. 
 * @return {string} A JSON response with all the data to send back to Alexa service.
 * 
 */
const get_help_topic = (handlerInput) => {

    let help_topic = help_command_helper.get_general_help_topic();

    if (alexa_utils.is_one_of_these_intents(handlerInput, 'SpecificHelpIntent')
     && !alexa_utils.is_slot_value_empty_from_handler_input(handlerInput, 'help_topic')) {
        let help_topic_id = alexa_utils.get_slot_id_from_handler_input(handlerInput, 'help_topic');
        help_topic = help_command_helper.get_help_topic_from_id(help_topic_id);
    }

    return help_topic;
}

module.exports = { handle_help_intent };