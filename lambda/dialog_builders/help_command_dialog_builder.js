const HELP_TOPIC_RESPONSES = {
    general: {
        summary: 'Esta skill sirve para acceder, mediante comandos de voz, a los informes de conducción de la empresa ADN Mobile.',
        action: 'Pídeme un informe en concreto indicándome su fecha y te recitaré todos los comentarios de tu evaluación.',
        sample: 'Por ejemplo, puedes decirme. Alexa, di a reportera ADN que me recite el informe de Abril de 2019.',
        suggestion: 'Si quieres ayuda más específica, pregúntame sobre el aspecto que quieres que te aclare.'
    },
    full_report: {
        title: 'Modo Informe Completo',
        summary: 'En este punto de la ayuda, te intentaré aclarar como tienes que indicarme que te recite el informe completo de golpe.',
        action: 'En este modo de lectura tienes que decirme que te lea el informe completo.',
        sample: 'Por ejemplo, puedes decirme. Alexa, di a reportera ADN que me recite todo el informe del tirón.',
        suggestion: ''
    },
    by_date: {
        title: 'Selección de Informe por fecha',
        summary: 'En este punto de la ayuda, te intentaré aclarar como puedes seleccionar un informe indicándome su fecha.',
        action: 'Para seleccionar un informe por su fecha, me tienes que indicar su año, mes y quincena.',
        sample: 'Por ejemplo, puedes decirme. Alexa, di a reportera ADN que me recite el informe de la primera quincena de mayo de 2019.',
        suggestion: ''
    },
    help_command: {
        title: 'Comando de ayuda',
        summary: 'En este punto de la ayuda, te intentaré aclarar como pedirme ayuda si te atascas y no sabes seguir utilizando la skill.',
        action: 'Simplemente pídeme ayuda sin más, o pregúntame por alguna funcionalidad concreta de la skill.',
        sample: 'Por ejemplo, puedes decirme. Alexa, di a reportera ADN que necesito ayuda con el informe completo.',
        suggestion: ''
    }
}

const HELP_END_QUESTION = '¿Quieres que te ayude con otra cosa?'

var help_command_dialog_builder = function() { };

/**
 * A method to build the speechText for a help command request. It's composed of six parts,
 *  The help topic summary text.
 *  The help topic action text.
 *  The help topic example text.
 *  The help topic suggestion text.
 *  A enumeration of its subtopics.
 *  An ending question text.
 * 
 * @param {string} help_topic - The internal help topic code.
 * 
 * @return {string} The speechText that's going to be included in alexa response.
 * 
 */
help_command_dialog_builder.prototype.build_help_topic_speech_text = (help_topic) => {

    console.log('dialog_builder - ' + help_topic);
    
    return build_help_topic_text_part(help_topic, 'summary') + ' ' +
        build_help_topic_text_part(help_topic, 'action') + ' ' +
        build_help_topic_text_part(help_topic, 'sample') + ' ' +
        build_help_topic_text_part(help_topic, 'suggestion') + ' ' +
        build_help_topic_subtopics_text(help_topic) + ' ' +
        build_help_ending_question_text();

}

/**
 * A private method to get the part of the speechText of the given help topic.
 * 
 * @param {string} help_topic - The internal help topic code.
 * @param {string} text_part - The code of the part of the speechText to retrieve.
 * 
 * @return {string} The part text that's going to be included in the speechText.
 * 
 */
const build_help_topic_text_part = (help_topic, text_part) => {

    return HELP_TOPIC_RESPONSES[help_topic][text_part];

}


const build_help_topic_subtopics_text = (help_topic) => {
    
    return '';

}

/**
 * A private method to get the ending question of the given help topic.
 * 
 * @return {string} The ending question that's going to be included in the speechText.
 * 
 */
const build_help_ending_question_text = () => {
    
    return HELP_END_QUESTION;

}

module.exports = new help_command_dialog_builder();