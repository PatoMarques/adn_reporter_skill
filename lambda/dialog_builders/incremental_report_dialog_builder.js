const ordinales = require('ordinales-js');

/**
 * A method to build the speechText for an incremental report request.
 *
 * @typedef {com: string} comment
 * @typedef {comment[]} report
 * 
 * @param {report} report - An object with all the comments of a report.
 * 
 * @return {string} The speechText that's going to be included in alexa response.
 * 
 */
const build_incremental_report_speech_text = (report_increment) => {

    return __build_full_speech_text(
        __build_report_increment_introduction(report_increment),
        __build_report_comments_text(report_increment.increment_data),
        __build_report_increment_ending(report_increment)
    );

}

module.exports = { build_incremental_report_speech_text };

/**
 * A internal method to generate a full speechText from
 *  an introduction, the report comments and an ending phrase.
 * 
 * @param {string} introduction - A string with the introductory phrase. 
 * @param {report} report - An object with all the comments of a report.
 * @param {string} ending - A string with the ending phrase.
 * 
 * @return {string} The speechText that's going to be included in alexa response.
 * 
 */
const __build_full_speech_text = (introduction, report_comments, ending) => {

    return `${introduction} ${report_comments} ${ending}`;

}

/**
 * A internal method to build the core of the speechText from the report comments.
 * 
 * @param {Report} report - A ReportDTO object with all related data.
 * 
 * @return {string} The speechText-core that's going to be included in alexa response.
 * 
 */
const __build_report_comments_text = (report) => {
    
    var report_comments_text = '';

    report.items.forEach(item => {
        report_comments_text += item.comment + ' ';
    });

    return report_comments_text;
}

/**
 * A internal method to build the introduction of the speechText for incremental report intent.
 * 
 * @param {ReportIncrement} report_increment - A ReportIncrement object with the necessary data to build a proper introduction.
 * 
 * @return {string} An introduction to insert in speechtext for incremental report intent.
 * 
 */
const __build_report_increment_introduction = (report_increment) => {

    var opening = 'Aquí tienes el incremento solicitado.';

    if (report_increment.repeat) opening = 'Escucha con atención esta vez.';
    else if (report_increment.is_rest_of_report) opening = 'Vale, pues como me has pedido, aquí tienes el resto del informe.';         

    var ordinal_indexes = [
        ordinales.toOrdinal(report_increment.increment_index),
        ordinales.toOrdinal(report_increment.updated_index)
    ]

    if (report_increment.increment_data.items.length == 1)
        opening = `${opening} Este incremento consta únicamente de un comentario.`;
    else
        opening = `${opening} Los comentarios incluidos en este incremento van del ${ordinal_indexes[0]} al ${ordinal_indexes[1]}.`;

    return opening;
}

/**
 * A internal method to build the ending of the speechText for incremental report intent.
 * 
 * @param {ReportIncrement} report_increment - A ReportIncrement object with the necessary data to build a proper ending.
 * 
 * @return {string} An ending to insert in speechtext for incremental report intent.
 * 
 */
const __build_report_increment_ending = (report_increment) => {

    var ending = `Todavía te quedan ${report_increment.rest_size} comentarios por escuchar. `

    if (report_increment.is_last_increment)
        ending = 'Bueno, pues esta es la última información que tengo para tí. Si quieres que te vuelva a evaluar, vuelve a solicitarme algún informe.';

    else ending += 'Si quieres seguir escuchando la evaluación, puedes decirme que continúe o incluso puedes pedirme que te cuente lo que quede del informe.';

    return ending;
}