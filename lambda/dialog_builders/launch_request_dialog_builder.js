/**
 * A method to build the speechText for the launch request.
 * 
 * @param {boolean} configured_account - A boolean with the result of checking if there's an api id configured in this alexa account.
 * 
 * @return {string} The speechText that's going to be included in alexa response.
 * 
 */
const build_launch_speech_text = (configured_account = true) => {

    return __build_full_speech_text(
        __build_speech_introduction(),
        __build_core_text(),
        __build_speech_ending(configured_account)
    );

}

module.exports = { build_launch_speech_text };

/**
 * A internal method to generate a full speechText from
 *  an introduction, the report core text and an ending phrase.
 * 
 * @param {string} introduction - A string with the introductory phrase. 
 * @param {report} report - An object with main information to tell.
 * @param {string} ending - A string with the ending phrase.
 * 
 * @return {string} The speechText that's going to be included in alexa response.
 * 
 */
const __build_full_speech_text = (introduction, report_comments, ending) => {

    return `${introduction} ${report_comments} ${ending}`;

}

/**
 * A internal method to build the core of the speechText that's ging to be a brief explanation of the skill common use flow.
 * 
 * @return {string} The speechText-core that's going to be included in alexa response.
 * 
 */
const __build_core_text = () => {

    var core_text = 'Para hacer uso de esta aplicación, tendrás que solicitarme un informe indicándome su fecha, y una vez ya lo haya encontrado, tendrás que indicarme un modo de acceso para interactuar con dicho informe.';
    

    return core_text;
}

/**
 * A internal method to build the introduction of the speechText for launch request.
 * 
 * @return {string} An introduction to insert in speechtext for launch request.
 * 
 */
const __build_speech_introduction = () => {

    var opening = 'Bienvenido a ADN Reporter. Mediante esta aplicación, podrás consultar los informes que publica la empresa ADN Mobile a través de comandos de voz.';

    return opening;
}

/**
 * A internal method to build the ending of the speechText for launch request.
 * 
 * @param {boolean} configured_account - A boolean indicating if there's an account configured in the skill.
 * 
 * @return {string} An ending to insert in speechtext for launch request.
 * 
 */
const __build_speech_ending = (configured_account) => {

    var ending = `Por ejemplo,  puedes comenzar diciendo ... di a reportera adn que quiero que me recite el informe de la primera quincena de mayo del año 2019 . `

    if (configured_account == false) ending = 'Parece ser que no tienes configurado tu perfil. Para acceder a cualquier funcionalidad, necesitas decirme tu número de conductor. Para ello dime algo como... di a reportera adn que quiero que mi id de usuario sea 5.'

    return ending;
}