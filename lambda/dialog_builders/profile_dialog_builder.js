
const build_set_user_api_id_speech_text = (user_api_id) => {

    return __build_full_speech_text(
        'Acabo de cambiar la configuración de tu cuenta.',
        __build_user_api_id_core_text(user_api_id),
        ''
    );

}

const build_get_user_api_id_speech_text = (user_api_id) => {

    return __build_full_speech_text(
        'Acabo de revisar la configuración de tu cuenta.',
        __build_user_api_id_core_text(user_api_id),
        ''
    );

}

const build_clear_profile_speech_text = () => {

    return __build_full_speech_text(
        'Acabo de borrar la configuración de tu cuenta.',
        'Acuérdate de volver a configurarla con un código de conductor antes de volver a interactuar conmigo.',
        ''
    );

}


module.exports = {  build_set_user_api_id_speech_text,
                    build_get_user_api_id_speech_text,
                    build_clear_profile_speech_text   };

                    

const __build_full_speech_text = (introduction, core_text, ending) => {

    return `${introduction} ${core_text} ${ending}`;

}

const __build_user_api_id_core_text = (user_api_id) => {
    
    return `En estos momentos, está configurada con el código de usuario ${user_api_id}.`
}