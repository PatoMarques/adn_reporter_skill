const ERROR_DIALOG_MESSAGES = {
    general: {
        speech: 'Lo siento, no he podido entender el comando. Por favor, inténtalo de nuevo.',
        reprompt: ''
    },
    access_api: {
        speech: 'Lo siento, parece que se ha producido un error en el acceso al servicio de la empresa ADN Mobile Solutions.',
        reprompt: ''
    },
    api_request: {
        speech: 'Lo siento, parece que se ha producido un error en la comunicación con el servicio de la empresa ADN Mobile Solutions. Es posible que se haya solicitado un informe que no existe, prueba a seleccionar un informe diferente.',
        reprompt: ''
    },
    requested_reports_not_available: {
        speech: 'Lo siento, parece que el informe solicitado no existe, prueba con otra fecha.',
        reprompt: ''
    },
    not_json_response: {
        speech: 'Lo siento, parece que no hay un error en el informe solicitado.',
        reprompt: ''
    },
    build_report_object: {
        speech: 'Lo siento, parece que no hay un error en el informe solicitado.',
        reprompt: ''
    },
    user_report_not_available: {
        speech: 'Lo siento, parece que no hay ningún informe asociado a tu identificador de usuario para la fecha indicada. Quizá no estuvieses registrado en el servicio de ADN Mobile en la fecha solicitada.',
        reprompt: ''
    },
    old_report_date: {
        speech: 'Lo siento, pero el servicio de ADN Mobile solamente da soporte a partir de TRALARI.',
        reprompt: ''
    },
    future_report_date: {
        speech: 'Lo siento, pero no soy adivina, no puedo evaluarte para una fecha futura, prueba con una fecha anterior a la actual.',
        reprompt: ''
    },
    date_invalid_year: {
        speech: 'Lo siento, pero no he podido entender la fecha proporcionada, parece ser que el año no es correcto.',
        reprompt: ''
    },
    date_invalid_month: {
        speech: 'Lo siento, pero no he podido entender la fecha proporcionada, parece ser que el mes no es correcto.',
        reprompt: ''
    },
    date_invalid_month_period: {
        speech: 'Lo siento, pero no he podido entender la fecha proporcionada, parece ser que la quincena no es correcta.',
        reprompt: ''
    },
    access_dynamo: {
        speech: 'Lo siento, parece que se ha producido un error en el acceso al servicio de la empresa ADN Mobile Solutions.',
        reprompt: ''
    },
    user_not_in_table: {
        speech: 'Lo siento, parece que no hay ningún usuario asociado a esta cuenta. Antes de poder acceder a cualquier informe debes de configurar tu código de conductor.',
        reprompt: ''
    },
    session_requested_report_invalid_format: {
        speech: 'Lo siento, parece que hay un error en el informe solicitado y no se ha guardado correctamente.',
        reprompt: ''
    },
    session_requested_report_not_exists: {
        speech: 'Lo siento, no recuerdo el informe que solicitaste...o quizá no lo llegaste a solicitar. Para acceder a un informe primero tienes que seleccionarlo.',
        reprompt: ''
    },
    access_s3_persistence: {
        speech: 'Lo siento, estamos teniendo problemas con el acceso a la base de datos. Disculpa las molestias.',
        reprompt: ''
    },
    s3_persistence_profile_not_exists: {
        speech: 'Parece ser que no tienes configurado tu perfil. Para acceder a cualquier funcionalidad, necesitas decirme tu número de conductor. Para ello dime algo como... di a reportera adn que quiero que mi id de usuario sea 5.',
        reprompt: ''
    }
}


const build_specific_error_speech_text = (error) => {
    
    let error_code = error.type && ERROR_DIALOG_MESSAGES.hasOwnProperty(error.type) ? error.type : 'general';

    return ERROR_DIALOG_MESSAGES[error_code].speech;

}

module.exports = { build_specific_error_speech_text };