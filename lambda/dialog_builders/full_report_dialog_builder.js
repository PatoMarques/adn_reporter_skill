

var full_report_dialog_builder = function() { };

/**
 * A method to build the speechText for a full report request.
 *
 * @typedef {com: string} comment
 * @typedef {comment[]} report
 * 
 * @param {report} report - An object with all the comments of a report.
 * 
 * @return {string} The speechText that's going to be included in alexa response.
 * 
 */
full_report_dialog_builder.prototype.build_full_report_speech_text = (report) => {

    return build_full_speech_text(
        `Este es tu informe completo. `,
        build_report_comments_text(report),
        ''
    );

}

module.exports = new full_report_dialog_builder();

/**
 * A internal method to generate a full speechText from
 *  an introduction, the report comments and an ending phrase.
 * 
 * @param {string} introduction - A string with the introductory phrase. 
 * @param {report} report - An object with all the comments of a report.
 * @param {string} ending - A string with the ending phrase.
 * 
 * @return {string} The speechText that's going to be included in alexa response.
 * 
 */
const build_full_speech_text = (introduction, report_comments, ending) => {

    return `${introduction} ${report_comments} ${ending}`;

}

/**
 * A internal method to build the core of the speechText from the report comments.
 * 
 * @param {Report} report - A ReportDTO object with all related data.
 * 
 * @return {string} The speechText-core that's going to be included in alexa response.
 * 
 */
const build_report_comments_text = (report) => {
    
    report_comments_text = '';

    report.items.forEach(item => {
        report_comments_text += item.comment + ' ';
    });

    return report_comments_text;
}