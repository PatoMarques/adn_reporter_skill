const date_helper = require('../helpers/date_helper');


const build_select_report_by_date_speech_text = (prepared_date, report_size) => {

    return __build_full_speech_text(
        __build_report_date_text(prepared_date),
        __build_report_access_options_text(report_size),
        ''
    );

}


module.exports = {build_select_report_by_date_speech_text};

const __build_full_speech_text = (introduction, report_comments, ending) => {

    return `${introduction} ${report_comments} ${ending}`;

}

const __build_report_access_options_text = (report_size) => {
    
    let report_access_options_text = `Este informe en concreto consta de ${report_size} comentarios. `
      if(report_size > 5) report_access_options_text += 'Uff, parece un informe bastante largo, ¿Cómo quieres que te lo cuente? ';
      report_access_options_text += 'Te lo puedo recitar todo de golpe, poco a poco, o incluso por tipo de comentarios, tu dirás.'

    return report_access_options_text;
}

const __build_report_date_text = (prepared_date) => {
    
    let month_period = date_helper.format_month_period(prepared_date.month_period);
    let month = date_helper.format_month(prepared_date.month);

    let date_introduction_text = `Pues ya tengo tu informe correspondiente a la ${month_period} quincena de ${month} del año ${prepared_date.year} en mis manos. `;

    return date_introduction_text;
}