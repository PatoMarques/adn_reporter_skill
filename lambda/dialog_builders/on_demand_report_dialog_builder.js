/**
 * A method to build the speechText for an on demand report request.
 *
 * @typedef {com: string} comment
 * @typedef {comment[]} report
 * 
 * @param {ReportOnDemand} on_demand_report - An object with all the comments of a report and other related data.
 * 
 * @return {string} The speechText that's going to be included in alexa response.
 * 
 */
const build_on_demand_report_speech_text = (on_demand_report) => {

    return __build_full_speech_text(
        __build_report_on_demand_introduction(on_demand_report),
        __build_report_comments_text(on_demand_report),
        __build_report_on_demand_ending(on_demand_report)
    );

}

module.exports = { build_on_demand_report_speech_text };

/**
 * A internal method to generate a full speechText from
 *  an introduction, the report comments and an ending phrase.
 * 
 * @param {string} introduction - A string with the introductory phrase. 
 * @param {report} report - An object with all the comments of a report.
 * @param {string} ending - A string with the ending phrase.
 * 
 * @return {string} The speechText that's going to be included in alexa response.
 * 
 */
const __build_full_speech_text = (introduction, report_comments, ending) => {

    return `${introduction} ${report_comments} ${ending}`;

}

/**
 * A internal method to build the core of the speechText from the report comments.
 * 
 * @param {Report} report - A ReportDTO object with all related data.
 * 
 * @return {string} The speechText-core that's going to be included in alexa response.
 * 
 */
const __build_report_comments_text = (on_demand_report) => {

    var on_demand_items = on_demand_report.on_demand_data.items;
    var report_comments_text = '';

    if (on_demand_items.length == 0) {
        report_comments_text += `Este informe consta de ${on_demand_report.group_by['GENERAL'].length} comentarios relacionados con tu rendimiento general, ${on_demand_report.group_by['MARCHAGLOBAL'].length} comentarios respecto a tu rendimiento estando el vehículo en marcha, ${on_demand_report.group_by['PARADAGLOBAL'].length} sobre situaciones del vehículo frenando, y ${on_demand_report.group_by['ARRANQUEGLOBAL'].length} relacionados con el arranque.`
    }
    else {
        on_demand_items.forEach(item => {
            report_comments_text += item.comment + '. ';
        });
    }

    return report_comments_text;
}

/**
 * A internal method to build the introduction of the speechText for incremental report intent.
 * 
 * @param {ReportOnDemand} on_demand_report - A ReportOnDemand object with the necessary data to build a proper introduction.
 * 
 * @return {string} An introduction to insert in speechtext for incremental report intent.
 * 
 */
const __build_report_on_demand_introduction = (on_demand_report) => {

    var opening = 'En el modo de acceso bajo demanda me puedes pedir las valoraciones de una categoría concreta.';

    opening = on_demand_report.on_demand_data.items.length > 0 ? 'Aquí tienes los comentarios solicitados.' : opening;

    if (on_demand_report.single_comment) opening = 'Aquí tienes el comentario que has solicitado.';

    if (on_demand_report.repeat) opening = 'Aquí tienes nuevamente los comentarios que te acabo de decir.';

    if (on_demand_report.rest) opening = 'Bueno, pues como has solicitado, aquí tengo el resto del informe.';

    return opening;
}

/**
 * A internal method to build the ending of the speechText for incremental report intent.
 * 
 * @param {ReportOnDemand} on_demand_report - A ReportIncrement object with the necessary data to build a proper ending.
 * 
 * @return {string} An ending to insert in speechtext for incremental report intent.
 * 
 */
const __build_report_on_demand_ending = (on_demand_report) => {

    var ending = `Todavía te quedan ${on_demand_report.rest_size} comentarios por escuchar. `

    if (on_demand_report.listened_comments.length == 0)
        ending += 'Puedes empezar por la evaluación general, que consta de cuatro comentarios.'

    else ending += `Si quieres seguir escuchando la evaluación, puedes preguntarme por ejemplo por los comentarios categorizados como ${on_demand_report.suggestions[0]}, o de forma más específica, por el tipo ${on_demand_report.suggestions[1]}.`;

    if (on_demand_report.is_last_on_demand) ending = 'Bueno, pues esto ha sido todo el informe, si quieres que te vuelva a evaluar, no dudes en pedirme uno nuevo.'

    return ending;
}