const fetch =  require("isomorphic-fetch");
const https = require('https');
const error_helper = require('../helpers/error_handling_helper')

const httpsAgent = new https.Agent({
    rejectUnauthorized: false,
});


/**
 * A method to call a web service API given its url through ssl connection
 *  ignoring rejected certificate and retrieve the response as a parsed json object.
 * 
 * @param {string} url - A string with the url where the API is listening to.
 * 
 * @return {*} typically an object with the parsed json response from api.
 * 
 */
const get_json_response_from_api = (url) => {
    return new Promise((resolve, reject) => {
        call_api_through_https(url)
        .then( response => {
            json_response = response.json();
            resolve(json_response);
        })
        .catch(err => {  
            reject(error_helper.throw_new_custom_error_if_not_handled(err, 'not_json_response', true));
        })
    })
}


/**
 * A internal method to call a web service API given its url through ssl connection
 *  ignoring rejected certificate.
 * 
 * @param {string} url - A string with the url where the API is listening to.
 * 
 * @return {fetch.Response} a response object with whatever comes in http response from api.
 * 
 */
const call_api_through_https = (url) => {

    request_options = {
        agent: httpsAgent
    }

    return new Promise((resolve, reject) => {
        call_api(url, request_options)
        .then( response => {
            resolve(response);
        })
        .catch(err => {
            reject(err);
        })
    })
}

/**
 * A method to call a web service API given a couple of parameters such the url inside the options parameter.
 * At this moment, it relies on javascript standard request library.
 * 
 * @param {string} url - A string with the url where the API is listening to.
 * @param {object} [request_options] - An object with a couple of parameters needed to connect to api.
 * @param {https.Agent} request_options.agent - A string with the url where the API is listening to.
 * 
 * @return {fetch.Response} typically an object with the parsed json response from api.
 * 
 */
const call_api = (url, request_options = {}) => {
    console.log('Preparando la llamada a la API');

    return new Promise((resolve, reject) => {

        console.log('Lanzando llamada a API');

        fetch(url, request_options)
        .then(response => {
            if(response.ok) {
                resolve(response)
            } else {
                throw error_helper.throw_new_custom_error('api_request', true);
            }
        })
        .catch(err => {
            reject(error_helper.throw_new_custom_error_if_not_handled(err, 'access_api', true));
        });
    });
}







module.exports = { get_json_response_from_api, call_api, call_api_through_https }