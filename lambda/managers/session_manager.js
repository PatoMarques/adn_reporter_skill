const error_helper = require('../helpers/error_handling_helper')


/**
 * A method to update any given attributes into alexa session object.
 * 
 * @param {ask_sdk.handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling a request.
 * 
 * @param {Object} new_attributes - An object with key-value pairs containing the attributes to update.
 * 
 * @return {ask_sdk.handlerInput} the updated handlerInput.
 * 
 */
const update_session_attributes = (handlerInput, new_attributes) => {

    let session_attributes = handlerInput.attributesManager.getSessionAttributes();

    Object.entries(new_attributes).forEach(([key, value]) => {
        session_attributes[key] = value;  
    });

    handlerInput.attributesManager.setSessionAttributes(session_attributes);

    return handlerInput;

}

/**
 * A method to get an attribute from alexa session object.
 * 
 * @param {ask_sdk.handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling a request.
 * 
 * @param {string} attr_name - A string with the name of the attribute to retrieve.
 * 
 * @return {*} the attribute value.
 * 
 */
const get_session_attribute = (handlerInput, attr_name) => {

    try {
        let session_attributes = handlerInput.attributesManager.getSessionAttributes();
        let attribute = session_attributes[attr_name];

        error_helper.throw_new_custom_error_if_condition('session_attr_not_exists', {'attribute': attribute});
    
        return attribute;
    }
    catch(err) {
        throw err;
    }

}



module.exports = { get_session_attribute, update_session_attributes };