const error_helper = require('../helpers/error_handling_helper')

/**
 * A method to update any given attributes into s3 persistance bucket.
 * 
 * @param {ask_sdk.handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling a request.
 * 
 * @param {Object} new_attributes - An object with key-value pairs containing the attributes to update.
 * 
 * @return {object} the updated attributes.
 * 
 */
const update_persistence_attributes = (handlerInput, new_attributes) => {

    let attributesManager = handlerInput.attributesManager;
    let s3_attributes = {};

    return new Promise((resolve, reject) => {
        attributesManager.getPersistentAttributes()
        .then( attributes => {
            s3_attributes = attributes || {};
            Object.entries(new_attributes).forEach(([key, value]) => {
                s3_attributes[key] = value;  
            });
            attributesManager.setPersistentAttributes(s3_attributes);
            attributesManager.savePersistentAttributes();
        })
        .then( () => {
            resolve(s3_attributes)
        })
        .catch( reject );
    })
    

}

/**
 * A method to set any given attributes into s3 persistance bucket.
 * 
 * @param {ask_sdk.handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling a request.
 * 
 * @param {Object} new_attributes - An object with key-value pairs containing the attributes to set.
 * 
 * @return {object} the updated attributes.
 * 
 */
const set_persistence_attributes = (handlerInput, new_attributes) => {

    let attributesManager = handlerInput.attributesManager;
    let s3_attributes = {};

    return new Promise((resolve, reject) => {
        attributesManager.setPersistentAttributes(s3_attributes);
        attributesManager.savePersistentAttributes()
        .then( () => {
            resolve(s3_attributes)
        })
        .catch( reject );
    })
    

}

/**
 * A method to get an attribute from s3 persistence bucket.
 * 
 * @param {ask_sdk.handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling a request.
 * 
 * @param {string} attr_name - A string with the name of the attribute to retrieve.
 * 
 * @return {*} the attribute value.
 * 
 */
const get_persistence_attribute = (handlerInput, attr_name) => {

        const attributesManager = handlerInput.attributesManager;

        return new Promise((resolve, reject) => {
            attributesManager.getPersistentAttributes()
            .then( attributes => {
                let s3_attributes = attributes || {};
                resolve(s3_attributes[attr_name] || null);
            })
            .catch(reject);
        })    

}




module.exports = { get_persistence_attribute, update_persistence_attributes, set_persistence_attributes };