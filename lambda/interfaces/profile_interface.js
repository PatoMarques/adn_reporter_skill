const profile_dao = require('../daos/profile_dao');
const profile_faker = require('../fakers/profile_faker');

const config_helper = require('../helpers/config_helper');

const get_user_api_id_from_persistence = (handlerInput, api_user_id) => {

    return new Promise((resolve, reject) => {
        config_helper.get_config_options()
        .then( config => {
            console.log('INTERFACE - ' + config['s3_persistence_fake'])
            if (config['s3_persistence_fake']) resolve(profile_faker.get_user_api_id_from_persistence(handlerInput, api_user_id));
            else resolve(profile_dao.get_user_api_id_from_persistence(handlerInput, api_user_id));
        })
        .catch(reject);
    });
}

const set_user_api_id_in_persistence = (handlerInput, api_user_id) => {
    
    return new Promise((resolve, reject) => {
        config_helper.get_config_options()
        .then( config => {
            if (config['s3_persistence_fake']) resolve(profile_faker.set_user_api_id_in_persistence(handlerInput, api_user_id));
            else resolve(profile_dao.set_user_api_id_in_persistence(handlerInput, api_user_id));
        })
        .catch(reject);
    });
}

const clear_profile_in_persistence = (handlerInput) => {
    
    return new Promise((resolve, reject) => {
        config_helper.get_config_options()
        .then( config => {
            if (config['s3_persistence_fake']) resolve(profile_faker.clear_profile_in_persistence(handlerInput));
            else resolve(profile_dao.clear_profile_in_persistence(handlerInput));
        })
        .catch(reject);
    });
}

module.exports = {get_user_api_id_from_persistence, set_user_api_id_in_persistence, clear_profile_in_persistence}