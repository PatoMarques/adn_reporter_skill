const report_dao = require('../daos/report_dao');
const report_faker = require('../fakers/report_faker');

const config_helper = require('../helpers/config_helper');

const get_reports_from_api = (prepared_date) => {

    return new Promise((resolve, reject) => {
        config_helper.get_config_options()
        .then( config => {
            if (config['api_fake']) resolve(report_faker.get_reports_from_api(prepared_date));
            else {
                resolve(report_dao.get_reports_from_api(prepared_date));
            }
        })
        .catch(reject);
    });
}

module.exports = {get_reports_from_api}