const file_helper = require('../helpers/file_helper')
const samples_path = `${__dirname}/samples/report_samples/`
const { build_multiple_report_dtos_from_api_response } = require('../helpers/report_helper');
const error_helper = require('../helpers/error_handling_helper');


/**
 * A internal method to get the fake reports array from file.
 * 
 * @typedef {year: number, month: number, month_period: number} prepared_date
 * 
 * @param {prepared_date} prepared_date - A dictionary with date parameters to select
 *  the right reports file.
 * 
 * @return {string} A JSON string with an array of fake reports from file.
 * 
 */
const get_reports_from_api = (prepared_date) => {

    let reports_file_path = __build_path_from_date(prepared_date)

    return new Promise((resolve, reject) => {
        file_helper.load_JSON_file_promisified(reports_file_path)
        .then(reports => {
            console.log('result taken successfully from faker.');
            reports = build_multiple_report_dtos_from_api_response(reports, prepared_date);
            resolve(reports);
        })
        .catch(err => {
            reject(error_helper.throw_new_custom_error_if_not_handled(err, 'build_report_object', true));
        });
    });
    
};


module.exports = {get_reports_from_api};


/**
 * A internal method to build the file path from date params.
 * 
 * @typedef {year: number, month: number, month_period: number} prepared_date
 * 
 * @param {prepared_date} prepared_date - A dictionary with date parameters to select
 *  the right reports file.
 * 
 * @return {string} A string with the path of the right path
 *  to the file with the fake reports.
 * 
 */
const __build_path_from_date = (prepared_date) => {
    let path = samples_path + `report`
    + `_${1}`
    + `_${5}`
    + `_${2019}_sample.json`;

    return path;
}