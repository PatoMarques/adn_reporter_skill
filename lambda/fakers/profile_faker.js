const file_helper = require('../helpers/file_helper')
const samples_path = `${__dirname}/samples/profile_samples/`


/**
 * A internal method to get the fake reports array from file.
 * 
 * @typedef {year: number, month: number, month_period: number} prepared_date
 * 
 * @param {prepared_date} prepared_date - A dictionary with date parameters to select
 *  the right reports file.
 * 
 * @return {string} A JSON string with an array of fake reports from file.
 * 
 */
const get_user_api_id_from_persistence = (handlerInput, amazon_user_id) => {

    var profile_file_path = samples_path + 'no_user_profile.json';
    console.log('FAKER - ' + profile_file_path)

    return new Promise((resolve, reject) => {
        file_helper.load_JSON_file_promisified(profile_file_path)
        .then(profile => {
            console.log('result taken successfully from faker.');
            resolve(profile && profile.hasOwnProperty('api_user') ? profile['api_user'] : null);
        })
        .catch(err => {
            console.log('error accessing reports from faker.')
            reject(err);
        });
    });
    
};

/**
 * A method to put profile's user api id in s3 persistence layer.
 * 
 * @param {ask_sdk.handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling a SetUserApiIdIntent request.
 * 
 * @param {number|string} api_user_id - A number or string with the user api id to set in persistence profile.
 * 
 * @return {object} An object with the account updated profile attributes.
 * 
 */
const set_user_api_id_in_persistence = (handlerInput, api_user_id) => {

    var profile_file_path = samples_path + 'no_user_profile.json';
    console.log('PROFILE_FAKER - ' + api_user_id);
    var s3Attributes = {};
    s3Attributes["api_user"] = api_user_id;

    return new Promise((resolve, reject) => {
        console.log('PROFILE_FAKER - ' + JSON.stringify(s3Attributes))
        file_helper.write_JSON_file_promisified(profile_file_path, s3Attributes)
        .then( updated_attributes => {
            resolve(updated_attributes);
        })
        .catch(reject);
    });
    
};

/**
 * A method to reset all attributes in s3 persistence layer.
 * 
 * @param {ask_sdk.handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling a SetUserApiIdIntent request.
 * 
 */
const clear_profile_in_persistence = (handlerInput) => {

    var profile_file_path = samples_path + 'no_user_profile.json';
    var s3Attributes = {};

    return new Promise((resolve, reject) => {
        console.log('PROFILE_FAKER - ' + JSON.stringify(s3Attributes))
        file_helper.write_JSON_file_promisified(profile_file_path, s3Attributes)
        .then( updated_attributes => {
            resolve(updated_attributes);
        })
        .catch(reject);
    });
    
};


module.exports = {get_user_api_id_from_persistence, set_user_api_id_in_persistence, clear_profile_in_persistence};