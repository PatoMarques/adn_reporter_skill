/**
 * A method to check if the current intent request is of one of the given intents.
 * 
 * @param {handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling a request.
 * 
 * @param {(string[]|string)} intent_names - An array of intent_names to check.
 * 
 * @return {boolean} A boolean with the result.
 * 
 */
const is_one_of_these_intents = (handlerInput, intent_names) => {

    intent_names = typeof(intent_names) === 'string' ? [intent_names] : intent_names;

    let is_an_intent_request = handlerInput.requestEnvelope.request.type === 'IntentRequest';
    
    return is_an_intent_request && 
        intent_names.includes(handlerInput.requestEnvelope.request.intent.name);

}

/**
 * A method to check if the current intent request is of one of the given intents.
 * 
 * @param {handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling a request.
 * 
 * @return {string} A string with the request intent name.
 * 
 */
const get_intent_type = (handlerInput) => {

    return handlerInput.requestEnvelope.request.intent.name;

}

/**
 * A method to build an alexa response with proper format from its params.
 * 
 * @param {ask_sdk.handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling a request.
 * 
 * @param {object} response_params - An object with the response params.
 * 
 * @return {string} A JSON string with the response.
 * 
 */
const build_json_response = (handlerInput, response_params) => {

    return handlerInput.responseBuilder
        .speak(response_params.speechText)
        .withShouldEndSession(response_params.should_end_session)
        .getResponse();

}

/**
 * A method to get the array of slots from the handlerInput with an alexa request data.
 * 
 * @param {ask_sdk.handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling a request.
 * 
 * @return {ask_sdk.slot[]} An array of slots from the current alexa request.
 * 
 */
const get_slots_from_handler_input = (handlerInput) => {

    let slots = handlerInput.requestEnvelope.request.intent.slots

    return slots;

}

/**
 * A method to check if the current a given slot is empty or has value, given the request slots array.
 * 
 * @param {ask_sdk.slot[]} slots - An array of objects representing slots from alexa request.
 * 
 * @param {string} slot_name - the name of the slot to check if it's in the slots array.
 * 
 * 
 * @return {boolean} A boolean with the result.
 * 
 */
const is_slot_value_empty = (slots, slot_name) => {

    return !(slots[slot_name].value);

}

/**
 * A method to get the slot id from slots array given its slot name.
 * 
 * @param {ask_sdk.slot[]} slots - An array of objects representing slots from alexa request.
 * 
 * @param {string} slot_name - the name of the slot to pick up its id.
 * 
 * 
 * @return {*} the id of the requested slot.
 * 
 */
const get_slot_id = (slots, slot_name) => {

    return slots[slot_name].resolutions
        .resolutionsPerAuthority[0].values[0].value.id;

}

/**
 * A method to get the slot ID from handlerInput object given its slot name.
 * 
 * @param {handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling a request.
 * 
 * @param {string} slot_name - the name of the slot to check if it's in the slots array.
 * 
 * 
 * @return {*} the id of the requested slot.
 * 
 */
const get_slot_id_from_handler_input = (handlerInput, slot_name) => {

    let slots = handlerInput.requestEnvelope.request.intent.slots

    return get_slot_id(slots, slot_name);

}

/**
 * A method to get the slot value from slots array given its slot name.
 * 
 * @param {ask_sdk.slot[]} slots - An array of objects representing slots from alexa request.
 * 
 * @param {string} slot_name - the name of the slot to check if it's in the slots array.
 * 
 * 
 * @return {*} the value of the requested slot.
 * 
 */
const get_slot_value = (slots, slot_name) => {

    return slots[slot_name].value;

}

/**
 * A method to get the slot value from handlerInput object given its slot name.
 * 
 * @param {ask_sdk.handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling a request.
 * 
 * @param {string} slot_name - the name of the slot to check if it's in the slots array.
 * 
 * 
 * @return {*} the value of the requested slot.
 * 
 */
const get_slot_value_from_handler_input = (handlerInput, slot_name) => {

    let slots = handlerInput.requestEnvelope.request.intent.slots

    return get_slot_value(slots, slot_name);

}

/**
 * A method to get the amazon user id from alexa request handler input.
 * 
 * @param {ask_sdk.handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling a request.
 * 
 * 
 * @return {string} the amazon user id inside handlerInput object.
 * 
 */
const get_amazon_user_id = (handlerInput) => {

    return handlerInput.requestEnvelope.context.System.user.userId;

}

module.exports = { build_json_response, get_slots_from_handler_input, is_slot_value_empty,
                   get_slot_id, get_amazon_user_id, get_slot_value_from_handler_input,
                   get_slot_value, is_one_of_these_intents, get_intent_type, get_slot_id_from_handler_input };