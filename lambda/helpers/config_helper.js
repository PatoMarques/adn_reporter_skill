const file_helper = require('./file_helper');

const config_path = `${__dirname}/../config.json`

const get_config_options = () => {

    return new Promise((resolve, reject) => {
        file_helper.load_JSON_file_promisified(config_path)
        .then(config => {
            resolve(config);
        })
        .catch(reject);
    });
}

module.exports = {get_config_options}