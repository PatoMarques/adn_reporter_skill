/**
 * A method to get the help topic using the id with the help text to include in the response.
 *
 * @param {number} help_topic_id - The id of the requested help topic.
 *  
 * @return {Object} A help topic with the text to include in response and the subtopics.
 * 
 */
const get_help_topic_from_id = (help_topic_id) => {
    
    help_topic = Object.keys(HELP_TOPICS).find(
        key => HELP_TOPICS[key]['interaction_model_id'] === parseInt(help_topic_id)
    )

    return help_topic;

}

/**
 * A method to get the sub topics inside a help topic object.
 *
 * @param {Object} help_topic - The help topic with the subtopics to get from.
 *  
 * @return {Object[]} An array of help topics that are sub topics of the given one.
 * 
 */
const get_help_sub_topics = (help_topic) => {

    return HELP_TOPICS[help_topic][sub_topics];

}

/**
 * A method to get the general help topic.
 *  
 * @return {Object} The general help topic object.
 * 
 */
const get_general_help_topic = () => {

    return 'general';

}

module.exports = { get_help_topic_from_id, get_help_sub_topics, get_general_help_topic };

const INTENT_DEFAULT_HELP_TOPICS = {
    GetReportByDateIntent: 'reading_modes',
    IncrementalReportIntent: 'incremental_report'
}

const HELP_TOPICS = {
    general: {
        interaction_model_id: 0,
        subtopics: ['full_report', 'help_command']
    },
    full_report: {
        interaction_model_id: 1,
        sub_topics: []
    },
    by_date: {
        interaction_model_id: 2,
        sub_topics: []
    },
    help_command: {
        interaction_model_id: 3,
        subtopics: []
    }
}