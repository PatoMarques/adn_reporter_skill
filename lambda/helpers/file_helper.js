'use strict';

const fs = require('fs');


/**
 * A method to read from a JSON file and parse it as a string.
 * 
 * @param {string} file_path - A string iwth the file's path to load.
 * 
 * @return {string} A JSON string with an array of fake reports from file.
 * 
 */
const load_JSON_file_promisified = (file_path) => {

  return new Promise((resolve, reject) => {
    try {
      var content = require(file_path);
      resolve(content);
    }catch(error) {
      reject(error)
    }
  });
    
};

/**
 * A method to read from a JSON file and parse it as a string.
 * 
 * @param {string} file_path - A string iwth the file's path to load.
 * 
 * @return {string} A JSON string with an array of fake reports from file.
 * 
 */
const write_JSON_file_promisified = (file_path, data) => {

  console.log('FILE_HELPER - ' + file_path);
  var str_data = JSON.stringify(data, null, 2);
  console.log('FILE_HELPER - ' + str_data);
  return new Promise((resolve, reject) => {
    fs.writeFile(file_path, str_data, (err) => {
      if (err) reject(err);
      console.log('Data written to file');
      resolve(data);
    });
  });
    
};

module.exports = { load_JSON_file_promisified, write_JSON_file_promisified };