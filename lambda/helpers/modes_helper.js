const alexa_helper = require('./alexa_helper');
const modes_dao = require('../daos/modes_dao');
const collections_helper = require('./collections_helper')

const MODES = {
    FULL_ACCESS: [
        'FullReportIntent'
    ],
    INCREMENTAL_ACCESS: [
        'IncrementalReportIntent',
        'AMAZON.NextIntent',
        'AMAZON.PreviousIntent',
        'RestOfCommentsIntent',
        'RepeatCommentsIntent'
    ],
    ON_DEMAND_ACCESS: [
        'OnDemandReportIntent',
        'OnDemandByTypeIntent',
        'OnDemandByTypeOneIntent',
        'RestOfCommentsIntent',
        'RepeatCommentsIntent'
    ]
}

const MODE_STARTER_INTENTS = [
    'FullReportIntent',
    'IncrementalReportIntent',
    'OnDemandReportIntent'
]


/**
 * A method to get the intent names that are related to a specific mode.
 * 
 * @param {string} mode - A string with the name of the mode to get intent names from.
 * 
 * @return {string[]} An array of strings with the names of the intents related to the given mode.
 * 
 */
const get_mode_intents = (mode) => {

    return MODES[mode];

}

/**
 * A method to get the intent names that are related to a specific mode.
 * 
 * @param {ask_sdk.handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling a request.
 * 
 * @param {string} mode - A string with the name of the mode to get intent names from.
 * 
 * @return {string[]} An array of strings with the names of the intents related to the given mode.
 * 
 */
const is_one_of_mode_related_intents = (handlerInput, mode) => {

    var result = alexa_helper.is_one_of_these_intents(handlerInput, get_mode_intents(mode));
    result = result &&  modes_dao.get_current_access_mode_from_session(handlerInput) == mode;

    return result;

}

/**
 * A method to get the intent names that start a new access mode in a given session.
 * 
 * @return {string[]} An array of strings with the names of the starter intents.
 * 
 */
const get_mode_starter_intents = () => {

    return MODE_STARTER_INTENTS;

}

/**
 * A method to get the intent names that are related to access modes.
 * 
 * @return {string[]} An array of strings with the names of the intents related to all access modes.
 * 
 */
const get_report_access_modes_intents = () => {

    var intents = collections_helper.merge_arrays_without_duplicates(MODES['FULL_ACCESS'], MODES['INCREMENTAL_ACCESS']);
    intents = collections_helper.merge_arrays_without_duplicates(intents, MODES['ON_DEMAND_ACCESS']);

    return intents;

}

/**
 * A method to get the access mode that it's going to be started at the beggining of the intent execution given its name.
 * 
 * @param {string} intent_name - A string with the intent name to pick the access mode it's related to.
 * 
 * @return {string} An string with the proper access mode.
 * 
 */
const get_access_mode_by_starter_intent_name = (intent_name) => {

    var access_mode = '';

    Object.entries(MODES).forEach(([mode, intents]) => {
        if (intents.includes(intent_name)){
            access_mode = mode;
        }
    })

    return access_mode;

}

module.exports = { get_mode_intents, is_one_of_mode_related_intents, 
                   get_report_access_modes_intents, get_mode_starter_intents,
                   get_access_mode_by_starter_intent_name };