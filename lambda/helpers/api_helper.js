const file_helper = require('./file_helper')

const config_path = `${__dirname}/../config.json`


/**
 * A method to call a web service API given a couple of parameters such the url inside the options parameter.
 * At this moment, it relies on javascript standard request library.
 * 
 * @param {object} prepared_date - An object with the date params to build the reports API url.
 * @param {number} prepared_date.year - A number with the year of the report to retrieve from API.
 * @param {number} prepared_date.month - A number with the month of the report to retrieve from API.
 * @param {number} prepared_date.month_period - A number with the motnh period of the report to
 *  retrieve from API. It should only be 1 if it's the first half of the month or 2 if it's the second.
 * 
 * @return {string} the built url with date params.
 * 
 */
const build_uri_from_date = async (prepared_date) => {
    return new Promise((resolve, reject) => {
        file_helper.load_JSON_file_promisified(config_path)
        .then( config => {
            let url = config['api_endpoint']
                        + `_${prepared_date.month_period}`
                        + `_${prepared_date.month}`
                        + `_${prepared_date.year}.json`
            console.log(url);
            resolve(url);
        })
        .catch(reject);
    });
}

module.exports = { build_uri_from_date }