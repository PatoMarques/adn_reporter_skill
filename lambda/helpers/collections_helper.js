const lodash = require('lodash')

const group_by = (array, key) => {
    return array.reduce((result, currentValue) => {
      (result[currentValue[key]] = result[currentValue[key]] || []).push(
        currentValue
      );
      
      return result;
    }, {});
  };

const group_indexes_by = (array, key) => {
  return array.reduce((result, currentValue, currentIndex) => {
    (result[currentValue[key]] = result[currentValue[key]] || []).push(
      currentIndex
    );
    
    return result;
  }, {});
};

const group_group_by_object_by_supergroups = (group_by_object, super_groups) => {
  var super_grouped_by = group_by_object;
  Object.entries(super_groups).forEach(([super_group, sub_groups]) => {
    super_grouped_by[super_group] = []
    sub_groups.forEach(sub_group => {
      super_grouped_by[super_group] = merge_arrays_without_duplicates(super_grouped_by[super_group], group_by_object[sub_group])
    })
  })

  return super_grouped_by
}

const get_elements_by_index_array = (array, indexArr) => {
  var indexed = [];
  indexArr.forEach(index => {
    indexed.push(array[index]);
  });

  return indexed;
};

const merge_arrays_without_duplicates = (array1, array2) => {
  var merged = array1.concat(array2);

  for(var i=0; i<merged.length; ++i) {
    for(var j=i+1; j<merged.length; ++j) {
        if(merged[i] === merged[j])
          merged.splice(j--, 1);
    }
  }

  return merged;
};

const get_max_from_group_by_object = (group_by_object, general_types) => {

  var suggestions = ['', '']
  var max_subtype = 0;
  var max_supertype = 0;

  Object.keys(general_types).forEach(type => {
    if (group_by_object[type].length > max_supertype) {
      max_supertype = group_by_object[type].length;
      suggestions[0] = type;
    }
  });

  general_types[suggestions[0]].forEach(sub_type => {
    if (group_by_object[sub_type].length > max_subtype) {
      max_subtype = group_by_object[sub_type].length;
      suggestions[1] = sub_type;
    }
  })

  return suggestions;
  
}

const delete_elements_by_array_of_indexes = (group_by_object, array_to_delete) => {
  Object.entries(group_by_object).forEach(([sub_type, indexes]) => {
    var new_indexes = indexes.slice();
    indexes.forEach(index => {
      if (array_to_delete.includes(index)) {
        new_indexes.splice(new_indexes.indexOf(index), 1);
      }
    group_by_object[sub_type] = new_indexes;
    })
  })

  return group_by_object;
}

const get_object_deep_clone = object => {
  return lodash.cloneDeep(object);
}

const get_remaining_indexes_from_array_to_delete = (full_array, array_to_delete) => {
  var rest = lodash.range(full_array.length);
  rest = lodash.difference(rest, array_to_delete);

  return rest;

}
  
module.exports = { group_by, group_indexes_by, 
                   get_elements_by_index_array,
                   merge_arrays_without_duplicates,
                   group_group_by_object_by_supergroups,
                   get_max_from_group_by_object,
                   delete_elements_by_array_of_indexes,
                   get_object_deep_clone,
                   get_remaining_indexes_from_array_to_delete }