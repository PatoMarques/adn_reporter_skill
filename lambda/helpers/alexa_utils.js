var alexa_utils = function() { };

/**
 * A method to build an alexa response with proper format from its params.
 * 
 * @typedef {Object} ResponseParams
 * @property {string} speechText - The speech of the response.
 * @property {boolean} should_end_session - A boolean to indicate if
 *  alexa has to end session or not.
 * 
 * @param {handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling a request.
 * 
 * @param {ResponseParams} response_params - An object with the response params.
 * 
 * @return {string} A JSON string with the response.
 * 
 */
alexa_utils.prototype.build_json_response = (handlerInput, response_params) => {

    return handlerInput.responseBuilder
        .speak(response_params.speechText)
        .withShouldEndSession(response_params.should_end_session)
        .getResponse();

}

/**
 * A method to check if the current intent request is of one of the given intents.
 * 
 * @param {handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling a request.
 * 
 * @param {(string[]|string)} intent_names - An array of intent_names to check.
 * 
 * @return {boolean} A boolean with the result.
 * 
 */
alexa_utils.prototype.is_one_of_these_intents = (handlerInput, intent_names) => {

    intent_names = typeof(intent_names) === 'string' ? [intent_names] : intent_names;

    let is_an_intent_request = handlerInput.requestEnvelope.request.type === 'IntentRequest';
    
    return is_an_intent_request && 
        intent_names.includes(handlerInput.requestEnvelope.request.intent.name);

}

/**
 * A method to update any given attributes into alexa session object.
 * 
 * @param {handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling a request.
 * 
 * @param {Object} intent_names - An object with key-value pairs containing the attributes to update.
 * 
 * @return {handlerInput} the updated handlerInput.
 * 
 */
alexa_utils.prototype.update_session_attributes = (handlerInput, new_attributes) => {

    let session_attributes = handlerInput.attributesManager.getSessionAttributes();

    Object.entries(new_attributes).forEach(([key, value]) => {
        session_attributes[key] = value;  
    });

    handlerInput.attributesManager.setSessionAttributes(session_attributes);

    return handlerInput;

}

/**
 * A method to get an attribute from alexa session object.
 * 
 * @param {handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling a request.
 * 
 * @param {string} attr_name - A string with the name of the attribute to retrieve.
 * 
 * @return {*} the attribute value.
 * 
 */
alexa_utils.prototype.get_session_attribute = (handlerInput, attr_name) => {

    let session_attributes = handlerInput.attributesManager.getSessionAttributes();
    let attribute = session_attributes[attr_name];

    return attribute;

}

/**
 * A method to check if the current a given slot is empty or has value, given the request slots array.
 * 
 * @param {slot[]} slots - An array of slots to check.
 * 
 * @param {string} slot_name - the name of the slot to check if it's in the slots array.
 * 
 * 
 * @return {boolean} A boolean with the result.
 * 
 */
alexa_utils.prototype.is_slot_value_empty = (slots, slot_name) => {

    return !(slots[slot_name].value);

}

/**
 * A method to check if the current a given slot is empty or has value, given the request handlerInput.
 * 
 * @param {handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling a request.
 * 
 * @param {string} slot_name - the name of the slot to check if it's in the slots array.
 * 
 * 
 * @return {boolean} A boolean with the result.
 * 
 */
alexa_utils.prototype.is_slot_value_empty_from_handler_input = (handlerInput, slot_name) => {

    slots = handlerInput.requestEnvelope.request.intent.slots

    return alexa_utils.prototype.is_slot_value_empty(slots, slot_name);

}

/**
 * A method to get the slot value from slots array given its slot name.
 * 
 * @param {slot[]} slots - An array of slots from alexa request.
 * 
 * @param {string} slot_name - the name of the slot to check if it's in the slots array.
 * 
 * 
 * @return {*} the value of the requested slot.
 * 
 */
alexa_utils.prototype.get_slot_value = (slots, slot_name) => {

    return slots[slot_name].value;

}

/**
 * A method to get the slot value from handlerInput object given its slot name.
 * 
 * @param {handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling a request.
 * 
 * @param {string} slot_name - the name of the slot to check if it's in the slots array.
 * 
 * 
 * @return {*} the value of the requested slot.
 * 
 */
alexa_utils.prototype.get_slot_value_from_handler_input = (handlerInput, slot_name) => {

    let slots = handlerInput.requestEnvelope.request.intent.slots

    return alexa_utils.prototype.get_slot_value(slots, slot_name);

}

/**
 * A method to get the slot id from slots array given its slot name.
 * 
 * @param {slot[]} slots - An array of slots from alexa request.
 * 
 * @param {string} slot_name - the name of the slot to check if it's in the slots array.
 * 
 * 
 * @return {*} the id of the requested slot.
 * 
 */
alexa_utils.prototype.get_slot_id = (slots, slot_name) => {

    return slots[slot_name].resolutions
        .resolutionsPerAuthority[0].values[0].value.id;

}

/**
 * A method to get the slot ID from handlerInput object given its slot name.
 * 
 * @param {handlerInput} handlerInput - A handlerInput object from ask-sdk module
 *  created when handling a request.
 * 
 * @param {string} slot_name - the name of the slot to check if it's in the slots array.
 * 
 * 
 * @return {*} the id of the requested slot.
 * 
 */
alexa_utils.prototype.get_slot_id_from_handler_input = (handlerInput, slot_name) => {

    let slots = handlerInput.requestEnvelope.request.intent.slots

    return alexa_utils.prototype.get_slot_id(slots, slot_name);

}


module.exports = new alexa_utils();