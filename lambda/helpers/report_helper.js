const Report = require('../dtos/report_dto');
const ReportItem = require('../dtos/report_item_dto');
const ReportDate = require('../dtos/report_date_dto');
const ReportIncrement = require('../dtos/report_increment_dto');
const ReportOnDemand = require('../dtos/report_on_demand_dto');

const collections_helper = require('./collections_helper');
const error_helper = require('./error_handling_helper');



const REPORT_FIELDS_IN_API_RESPONSE = [
    ['idUserdriv', 'IdUserdriv'],
    ['customerId', 'CustomerId'],
    ['startDate', 'StartDate'],
    ['endDate', 'EndDate'],
    ['idRule', 'IdRule'],
    ['ruleType', 'RuleType'],
    ['com', 'Com'],
    ['geoLat', 'GeoLat'],
    ['geoLong', 'GeoLong']
]

const REPORT_GROUPS = {
    GENERAL: ['Global', 'EvolucionPeriodoAnteriorGlobal', 'EvolucionNPeriodosGlobal', 'Animar'],
    PARADAGLOBAL: ['Parada', 'ClimaParada', 'PosicionParada'],
    MARCHAGLOBAL: ['Marcha', 'ClimaMarcha', 'PosicionMarcha'],
    ARRANQUEGLOBAL: ['Arranque', 'ClimaArranque', 'PosicionArranque'],
    CLIMAGENERAL: ['ClimaParada', 'ClimaMarcha', 'ClimaArranque'],
    POSICIÓNGENERAL: ['PosicionParada', 'PosicionMarcha', 'PosicionArranque'],
    SECOGENERAL: ['Parada', 'Arranque', 'Marcha']
}

const DEFAULT_REPORT_INCREMENT_SIZE = 3;

/**
 * A method to build one Report object for each report data retrieved from api.
 * 
 * @param {object} prepared_date - An object with the date parameters to compose the url of the requested report.
 * @param {number} prepared_date.year - A number indicating the year of the reports to retrieve.
 * @param {number} prepared_date.month - A number indicating the month of the reports to retrieve, starting from 1 for January.
 * @param {number} prepared_date.month_period - A number indicating the period of the month of the reports to retrieve,
 *  being 1 the first half, and 2 the second half.
 * 
 * @param {object[]} reports - A collection of report items in raw api format.
 * 
 * @return {Report[]} A collection of Report objects of one date in particular built from the raw report items.
 * 
 */
const build_multiple_report_dtos_from_api_response = (reports, prepared_date) => {

    reports = __preprocess_report_items_from_api_response(reports);
    report_dtos = [];
    report_items_grouped_by_user = collections_helper.group_by(reports, 'idUserdriv');
    Object.keys(report_items_grouped_by_user).forEach(user => {
        report_dtos.push(
            __build_report_dto_from_api_response(
                report_items_grouped_by_user[user],prepared_date, user
            )
        );
    })

    return report_dtos;

}


/**
 * A method to build one Report object from a json formatted report inside the alexa session.
 * 
 * @param {string} json_report - An object formatted as a JSON string to store in session.
 * 
 * @return {Report} A Report object retrieved from alexa session.
 * 
 */
const build_report_dto_from_json = (json_report) => {

    parsed_report = JSON.parse(json_report);
    report = __build_report_dto_from_session(parsed_report);

    return report;

}

/**
 * A method to retrieve a the requested report given the date and the specific user id in the api side.
 * 
 * @param {object} prepared_date - An object with the date parameters to compose the url of the requested report.
 * @param {number} prepared_date.year - A number indicating the year of the reports to retrieve.
 * @param {number} prepared_date.month - A number indicating the month of the reports to retrieve, starting from 1 for January.
 * @param {number} prepared_date.month_period - A number indicating the period of the month of the reports to retrieve,
 *  being 1 the first half, and 2 the second half.
 * 
 * @param {number} upi_user_id - A number indicating the id of the user in the api.
 * 
 * @return {Report} A Report object of one date and one specific user in particular.
 * 
 */
const get_user_specific_report_from_api = (reports, api_user_id) => {

        try{
            let user_report = reports.filter(report => report.user_api_id == api_user_id)[0];
            error_helper.throw_new_custom_error_if_condition('user_report_not_available', params={user_report: user_report}, false);
            return user_report;
        }catch(err){
            throw err;
        }

}

/**
 * A method to transform one Report object to json format to store in alexa session.
 * 
 * @param {Report} report - An object formatted as a JSON string to store in session.
 * 
 * @return {string} A Report object formatted as string.
 * 
 */
const transform_report_dto_to_json = (report) => {

    json_report = JSON.stringify(report);

    return json_report;

}

/**
 * A method to get an increment of a given report dto object, specifying index.
 * 
 * @param {Report} report - A report dto object to slice and retrieve the desired increment from.
 * 
 * @param {number} index - An integer with the index of the increment to slice from.
 * 
 * @param {boolean} repeat - A boolean meaning that the retrieved increment should be the same as last time,
 *  so no updating the index for the next time.
 * 
 * @param {number} increment_size - An integer with the size of the increment to retrieve.
 * 
 * @param {boolean} rest_of_increment - A boolean meaning that the retrieved increment should be the rest of the report.
 * 
 * @param {boolean} previous - A boolean meaning that the retrieved increment should be previous as last time.
 * 
 * @return {object} An object with the increment data, the updated index and a boolean indicating if it's the last increment.
 * 
 */
const get_report_increment = (report, 
    {
        index = 0, repeat = false, increment_size = DEFAULT_REPORT_INCREMENT_SIZE,
        rest_of_increment = false, previous = false
    } = {}
) => {

    if (repeat) index -= increment_size;
    if (previous) index -= increment_size * 2;

    var params = { 'repeat': repeat, 'increment_index': index + 1, 'is_rest_of_report': rest_of_increment };

    params['updated_index'] = rest_of_increment ? report.items.length : index + increment_size;
    params['is_last_increment'] = params['updated_index'] >= (report.items.length);
    if (params['is_last_increment']) params['updated_index'] = report.items.length;
    params['rest_size'] = report.items.length - params['updated_index'];
    
    var report_increment_data = report;
    report_increment_data.items = report_increment_data.items.slice(index, params['updated_index']);


    return new ReportIncrement( report_increment_data, params )
    
};

/**
 * A method to get an increment of a given report dto object, specifying index.
 * 
 * @param {Report} report - A report dto object to slice and retrieve the desired increment from.
 * 
 * @param {number[]} listened_reports - An integer with the index of the increment to slice from.
 * 
 * @return {object} An object with the increment data, the updated index and a boolean indicating if it's the last increment.
 * 
 */
const get_on_demand_data = (report, 
    {
        listened_comments = [],
        requested_type = '',
        single_comment = false,
        requested_items = [],
        repeat = false,
        rest = false
    } = {}
) => {
    var params = { 'requested_group': requested_type, 'single_comment': single_comment, 'requested_items': requested_items, 'rest': rest, 'repeat': repeat };

    params['group_by'] = __categorize_report_items_by_type(report.items, listened_comments);
    
    if (rest) {
        params['requested_items'] = collections_helper.get_remaining_indexes_from_array_to_delete(report.items, listened_comments);
    } else if (requested_items.length == 0) {
        params['requested_items'] = params['single_comment'] ? [params['group_by'][requested_type][0]] : params['group_by'][requested_type];
    }
    
    params['listened_comments'] = collections_helper.merge_arrays_without_duplicates(listened_comments, params['requested_items'] || []);
    params['group_by'] = collections_helper.delete_elements_by_array_of_indexes(params['group_by'], params['listened_comments']);

    var on_demand_report = __set_on_demand_report(report, params);
    params = __set_additional_parameters(params, report, on_demand_report);
 
    return new ReportOnDemand( on_demand_report, params );
    
};



module.exports = { 
    build_multiple_report_dtos_from_api_response,
    get_user_specific_report_from_api,
    transform_report_dto_to_json, 
    build_report_dto_from_json,
    get_report_increment,
    get_on_demand_data
}




/**
 * A method to build one Report object for a single report data retrieved from api.
 * 
 * @param {object} prepared_date - An object with the date parameters to compose the url of the requested report.
 * @param {number} prepared_date.year - A number indicating the year of the reports to retrieve.
 * @param {number} prepared_date.month - A number indicating the month of the reports to retrieve, starting from 1 for January.
 * @param {number} prepared_date.month_period - A number indicating the period of the month of the reports to retrieve,
 *  being 1 the first half, and 2 the second half.
 * 
 * @param {object[]} report_items - A collection of report items in raw api format.
 * 
 * @param {number} user_id - A number indicating the id of the user in api.
 * 
 * @return {Report} A Report object of one date and user in particular built from the raw report items.
 * 
 */
const __build_report_dto_from_api_response = (report_items, prepared_date, user_id) => {
    client_id = report_items[0].customerId;
    report_date = new ReportDate(
        prepared_date.year,
        prepared_date.month,
        prepared_date.month_period
    );

    return new Report(
        user_id,
        client_id,
        report_date,
        __build_report_items_from_api_response(report_items)
    );
}

/**
 * A method to build a collection of ReportItem objects from a single report data retrieved from api.
 * 
 * @param {object[]} report_items - A collection of report items in raw api format.
 * 
 * @return {ReportItem[]} A collection of ReportItem objects from a single report to include in its Report object.
 * 
 */
const __build_report_items_from_api_response = (report_items) => {
    report_item_dtos = [];
    report_items.forEach(item => {
        report_item_dtos.push(new ReportItem(
            item.idRule,
            item.ruleType,
            item.com,
            item.geoLat,
            item.geoLong
        ))
    });

    return report_item_dtos;
}

/**
 * A method to preprocess the data sent from api. That's because in some reports, the old ones in particular,
 *  it's been noted that the report name fields are capitalized while the new ones not. So this method creates new objects
 *  with the non capitalized attribute names wathever the original format is.
 * 
 * @param {object[]} report_items - A collection of report items in raw api format.
 * 
 * @return {ReportItem[]} A collection of ReportItem objects from a single report to include in its Report object.
 * 
 */
const __preprocess_report_items_from_api_response = (report_items) => {
    preprocessed_report_items = []
    report_items.forEach(item => {
        preprocessed_item = {}
        REPORT_FIELDS_IN_API_RESPONSE.forEach(field => {
            field.forEach(possible_name => {
                if (possible_name in item){
                    preprocessed_item[field[0]] = item[possible_name];
                    return;
                }
            })
        })
        preprocessed_report_items.push(preprocessed_item);
    })

    return preprocessed_report_items;
}

/**
 * A method to build one Report object for a single report data retrieved from api.
 * 
 * @param {object} parsed_json_report - An object with a parsed json representation of a ReportDTO Object.
 * 
 * @return {Report} A Report object built upon the generic object generated from parsing a JSON string with the data.
 * 
 */
const __build_report_dto_from_session = (parsed_json_report) => {
    client_id = parsed_json_report.client_id;
    report_date = new ReportDate(
        parsed_json_report.prepared_date.year,
        parsed_json_report.prepared_date.month,
        parsed_json_report.prepared_date.month_period
    );
    
    return new Report(
        parsed_json_report.user_api_id,
        parsed_json_report.client_api_id,
        report_date,
        __build_report_items_from_session(parsed_json_report.items)
    );
}

/**
 * A method to build a collection of ReportItem objects from a single report data retrieved from api.
 * 
 * @param {object[]} parsed_json_report_items - A collection of report items in parsed json format.
 * 
 * @return {ReportItem[]} A collection of ReportItem objects from a single report to include in its Report object.
 * 
 */
const __build_report_items_from_session = (parsed_json_report_items) => {
    report_item_dtos = [];
    parsed_json_report_items.forEach(item => {
        report_item_dtos.push(new ReportItem(
            item.rule_id,
            item.rule_type,
            item.comment,
            item.coords_lat,
            item.coords_long
        ))
    });

    return report_item_dtos;
}

/**
 * An internal method to separate an array of report items by its ruletype.
 * 
 * @param {ReportItem[]} report_items - A collection of report items in parsed json format.
 * 
 * @return {object} An object with one property per valid ruletype and an array of the corresponding categorized item indexes.
 * 
 */
const __categorize_report_items_by_type = (report_items, listened_comments) => {
    var categorized = collections_helper.group_indexes_by(report_items, 'rule_type');
    categorized = collections_helper.group_group_by_object_by_supergroups(categorized, REPORT_GROUPS);
    categorized = collections_helper.delete_elements_by_array_of_indexes(categorized, listened_comments);

    return categorized;
}

/**
 * An internal method to prepare a report object with only the on demand requested comments.
 * 
 * @param {Report} full_report - An object with the current session full report.
 * 
 * @param {Object} params - An object with some params needed to select the on demand items.
 * 
 * @return {Report} An object of type Report with a truncated array of items with only the selected by user.
 * 
 */
const __set_on_demand_report = (full_report, params) => {
    var on_demand_report = collections_helper.get_object_deep_clone(full_report);
    var on_demand_items = [];

    if (params['requested_items'] && params['requested_items'].length > 0){
        on_demand_items = collections_helper.get_elements_by_index_array(on_demand_report.items, params['requested_items']);
    }
    on_demand_report.items = on_demand_items;

    return on_demand_report;
}

/**
 * An internal method to set the additional params.
 * 
 * @param {Object} params - An object with some params object to fill with additional parameters to pass to dialog builder.
 * 
 * @param {Report} full_report - An object with the current session full report.
 * 
 * @param {Report} on_demand_report - An object of type Report with the selected items.
 * 
 * 
 * @return {Report} An object with all the params already settled to insert in DTO.
 * 
 */
const __set_additional_parameters = (params, full_report, on_demand_report) => {

    params['rest_size'] = full_report.items.length - params['listened_comments'].length;
    
    if (params['rest_size'] == 0) {
        params['is_last_on_demand'] = true
    } else {
        params['suggestions'] = collections_helper.get_max_from_group_by_object(params['group_by'], REPORT_GROUPS);
    }
    
    params['single_comment'] = on_demand_report.items.length == 1;

    return params;
}