const CUSTOM_ERRORS = {
    api_request: {
        condition: (params) => (true),
        message: 'Error en petición a la api. Status response different than 200.'
    },
    access_api: {
        condition: (params) => (true),
        message: 'Error en acceso a api. Servidor inaccesible.'
    },
    requested_reports_not_available: {
        condition: (params) => (!Array.isArray(params.reports)),
        message: 'Error en acceso a los informes. No existe el informe para la fecha indicada.'
    },
    not_json_response: {
        condition: (params) => (true),
        message: 'Error intentando parsear la respuesta. JSON no válido.'
    },
    throw_report_object: {
        condition: (params) => (true),
        message: 'Error en la construcción del informe. Fallo en formato de la respuesta.'
    },
    user_report_not_available: {
        condition: (params) => (!params.user_report),
        message: 'Error en selección del informe. No existe un informe del usuario para la fecha indicada.'
    },
    old_report_date: {
        condition: (params) => (params.requested_date - 0 <= 0),
        message: 'Error en fecha indicada. Es anterior a la soportada por el servicio.'
    },
    future_report_date: {
        condition: (params) => (params.requested_date - new Date() > 0),
        message: 'Error en fecha indicada. Es posterior a la actual.'
    },
    date_invalid_year: {
        condition: (params) => (params.prepared_year < 0),
        message: 'Error en fecha indicada. Parámetro año inválido.'
    },
    date_invalid_month: {
        condition: (params) => (parseInt(params.prepared_month) > 12 || parseInt(params.prepared_month) < 1),
        message: 'Error en fecha indicada. Parámetro mes inválido.'
    },
    date_invalid_month_period: {
        condition: (params) => (parseInt(params.prepared_month_period) > 2 || parseInt(params.prepared_month_period) < 1),
        message: 'Error en fecha indicada. Parámetro quincena inválido.'
    },
    access_dynamo: {
        condition: (params) => (true),
        message: 'Error en acceso a dynamodb.'
    },
    user_not_in_table: {
        condition: (params) => (params.profile !== undefined && params.profile !== null),
        message: 'Error en acceso a dynamodb.'
    },
    session_attr_not_exists: {
        condition: (params) => (!params.attribute),
        message: 'Error en acceso a sesión. Atributo no existe.'
    },
    session_requested_report_invalid_format: {
        condition: (params) => (true),
        message: 'Error en acceso a sesión. Formato del informe incorrecto.'
    },
    session_requested_report_not_exists: {
        condition: (params) => (true),
        message: 'Error en acceso a sesión. No hay informe solicitado en sesión.'
    },
    access_s3_persistence: {
        condition: (params) => (true),
        message: 'Error en acceso a s3 persistence. Servidor no responde.'
    },
    s3_persistence_profile_not_exists: {
        condition: (params) => (true),
        message: 'Error en acceso a sesión. No hay ningún perfil almacenado en s3 persistence.'
    }
};



/**
 * A method to build and throw/return a new custom error of specified type based
 *  on the result of the condition evaluated with the params_to_check.
 * 
 * @param {string} error_type - A string with the code of the error to raise or return.
 * 
 * @param {object} params_to_check - An object with the params to evaluate with the condition
 *  stored in CUSTOM_ERRORS dict.
 * 
 * @param {boolean} promisified - A boolean indicating if we want to raise or return the built error.
 *  If promisified = true, the method returns de error, if not, it throws it.
 * 
 * 
 * @return {Error} An object of type Error with type property set to error_type. Only returns if promisified = true.
 * 
 */
const throw_new_custom_error_if_condition = (error_type, params_to_check, promisified = false) => {

    try{
        if (_check_error_condition(error_type, params_to_check)) return throw_new_custom_error(error_type, promisified);
    }
    catch(err){
        throw err;
    }

}

/**
 * A method to build and throw/return a new custom error of specified type
 *  if the error catched and passed as argument to this method isn's one of the defined CUSTOM ERRORS.
 * 
 * @param {Error} error - An object of type Error to check if it's one of the defined CUSTOM ERRORS.
 * 
 * @param {string} error_type - A string with the code of the error to raise or return.
 * 
 * @param {boolean} promisified - A boolean indicating if we want to raise or return the built error.
 *  If promisified = true, the method returns de error, if not, it throws it.
 * 
 * 
 * @return {Error} An object of type Error with type property set to error_type. Only returns if promisified = true.
 * 
 */
const throw_new_custom_error_if_not_handled = (error, error_type, promisified = false) => {

    try{
        if (_check_if_already_handled_error(error)) return error;
        return throw_new_custom_error(error_type, promisified);
    }
    catch(err){
        throw err;
    }

}

/**
 * A method to build and throw/return a new custom error of specified type in new_error_type param
 *  if the error catched and passed as argument to this method has a type specified in error_type param.
 * 
 * @param {Error} error - An object of type Error to check if it's one of the defined CUSTOM ERRORS.
 * 
 * @param {string} error_type - A string with the code of the error to check.
 * 
 * @param {string} new_error_type - A string with the code of the error to raise or return.
 * 
 * @param {boolean} promisified - A boolean indicating if we want to raise or return the built error.
 *  If promisified = true, the method returns de error, if not, it throws it.
 * 
 * 
 * @return {Error} An object of type Error with type property set to error_type. Only returns if promisified = true.
 * 
 */
const throw_new_custom_error_if_error_type = (error, error_type, new_error_type, promisified = false) => {

    try{
        if (error.type === error_type) return throw_new_custom_error(new_error_type, promisified);
    }
    catch(err){
        throw err;
    }

}

/**
 * A method to build and throw/return a new custom error of specified type in error_type param.
 * 
 * @param {string} error_type - A string with the code of the error to check.
 * 
 * @param {boolean} promisified - A boolean indicating if we want to raise or return the built error.
 *  If promisified = true, the method returns de error, if not, it throws it.
 * 
 * 
 * @return {Error} An object of type Error with type property set to error_type. Only returns if promisified = true.
 * 
 */
const throw_new_custom_error = (error_type, promisified=false) => {

    error = _build_new_custom_error(error_type);
    if (promisified) {
        return error;
    }
    throw error;

}



/**
 * An internal method to build a new custom error of specified type in error_type param and print info in console.
 * 
 * @param {string} error_type - A string with the code of the error to check.
 * 
 * 
 * @return {Error} An object of type Error with type property set to error_type.
 * 
 */
const _build_new_custom_error = (error_type) => {

    let error = new Error(CUSTOM_ERRORS[error_type].message);
    error.type = error_type;
    _print_error_information(error);

    return error;

}

/**
 * A method to check if the params_to_check matches with the specific error type condition stored in object CUSTOM_ERRORS.
 * 
 * @param {string} error_type - A string with the code of the error to check.
 * 
 * @param {object} params_to_check - An object with the params to evaluate with the condition
 *  stored in CUSTOM_ERRORS dict.
 * 
 * 
 * @return {boolean} A boolean with the result of evaluation.
 * 
 */
const _check_error_condition = (error_type, params_to_check) => {

    return CUSTOM_ERRORS[error_type].condition(params_to_check);

}

/**
 * A method to check if the given error has type and it's one of the CUSTOM ERRORS.
 * 
 * @param {Error} error - An object of type Error to check if it's one of the defined CUSTOM ERRORS.
 * 
 * 
 * @return {boolean} A boolean with the result of evaluation.
 * 
 */
const _check_if_already_handled_error = (error) => {

    return error.type && CUSTOM_ERRORS.hasOwnProperty(error.type);

}

/**
 * A method to print in console the trace and details of the given error.
 * 
 * @param {Error} error - An object of type Error to print.
 * 
 * 
 */
const _print_error_information = (error) => {

    console.error(`Error ${error.type} handled: ${error.message}`);
    console.trace(error);

}





module.exports = { throw_new_custom_error,
                   throw_new_custom_error_if_condition, 
                   throw_new_custom_error_if_not_handled,
                   throw_new_custom_error_if_error_type };