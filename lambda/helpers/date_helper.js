const moment = require('moment');
const { is_slot_value_empty, get_slot_id } = require('./alexa_helper');
const error_helper = require('./error_handling_helper')


/**
 * A method to preprocess the date params the user gives us inside the alexa intent request.
 * 
 * @param {ask_sdk.slot[]} report_date_slots - An array of objects representing slots from alexa request containing date params.
 * 
 * @return {object} An object with three attributes one for each date part required in api call.
 * 
 */
const preprocess_report_date = (report_date_slots) => {

    try{
        let prepared_date = {};

        prepared_date.year = __preprocess_report_year(report_date_slots);
        prepared_date.month = __preprocess_report_month(report_date_slots);
        prepared_date.month_period = __preprocess_report_month_period(report_date_slots);
    
        console.log(prepared_date);
        error_helper.throw_new_custom_error_if_condition('future_report_date', 
                                                         {requested_date: get_full_date_from_prepared_date(prepared_date)});

        error_helper.throw_new_custom_error_if_condition('old_report_date', 
                                                         {requested_date: get_full_date_from_prepared_date(prepared_date)});
    
        return prepared_date;
    }
    catch(err){
        throw err;
    }
}

/**
 * A method to create a Date object from a prepared_date object built in preprocess_report_date function.
 * 
 * @param {object} prepared_date - An object with the date parameters to compose the url of the requested report.
 * @param {number} prepared_date.year - A number indicating the year of the reports to retrieve.
 * @param {number} prepared_date.month - A number indicating the month of the reports to retrieve, starting from 1 for January.
 * @param {number} prepared_date.month_period - A number indicating the period of the month of the reports to retrieve,
 *  being 1 the first half, and 2 the second half.
 * 
 * @return {Date} A javascript Date object built from date params.
 * 
 */
const get_full_date_from_prepared_date = (prepared_date) => {

    return new Date(prepared_date.year,
                    prepared_date.month -1,
                    (prepared_date.month_period -1) * 15);

}

/**
 * A method to format a month period number in api format to string to include in speechText.
 * 
 * @param {number} month_period - A month period in api number format to format to string.
 * 
 * @return {string} A string representation of a month period number in api format to be included in speechText.
 * 
 */
const format_month_period = (month_period) => {
    
    return month_period == 1 ? 'primera' : 'segunda';

}

/**
 * A method to format a month number in api format to string to include in speechText.
 * 
 * @param {number} month - A month in api number format to format to string.
 * 
 * @return {string} A string representation of a month period number in api format to be included in speechText.
 * 
 */
const format_month = (month_number) => {
    moment.locale('es');
    return moment().month(month_number - 1).format('MMMM');
}



module.exports = { preprocess_report_date, get_full_date_from_prepared_date, format_month, format_month_period };


/**
 * An internal method to preprocess the date params the user gives us inside the alexa intent request and give back the year in api format.
 * 
 * @param {ask_sdk.slot[]} report_date_slots - An array of objects representing slots from alexa request containing date params.
 * 
 * @return {number} A number representing the year of the report in YYYY format.
 * 
 */
const __preprocess_report_year = (report_date_slots) => {

    let prepared_year = '';

    try{
        if (is_slot_value_empty(report_date_slots, 'report_year')) {
            prepared_year = new Date().getFullYear();
        } else {
            prepared_year = report_date_slots.report_year.value;
        }

        error_helper.throw_new_custom_error_if_condition('date_invalid_year', 
                                                        {'prepared_year': prepared_year});
    }
    catch(err){ throw err; }
    
    return parseInt(prepared_year);
}

/**
 * An internal method to preprocess the date params the user gives us inside the alexa intent request and give back the month in api format.
 * 
 * @param {ask_sdk.slot[]} report_date_slots - An array of objects representing slots from alexa request containing date params.
 * 
 * @return {number} A number representing the month of the report being 1 for January and 12 for December.
 * 
 */
const __preprocess_report_month = (report_date_slots) => {

    let prepared_month = '';

    try {
        if(!is_slot_value_empty(report_date_slots, 'report_month'))
            prepared_month = get_slot_id(report_date_slots, 'report_month');
        else if(__is_first_month_of_year(report_date_slots)) prepared_month = 1;
        else if (__is_last_month_of_year(report_date_slots)) prepared_month = 12;
        else prepared_month = new Date().getMonth() + 1;

        error_helper.throw_new_custom_error_if_condition('date_invalid_month', 
                                                         {'prepared_month': prepared_month});
    }
    catch(err) { throw err; }
    
    return parseInt(prepared_month);;
}


/**
 * An internal method to preprocess the date params the user gives us inside the alexa intent request and give back the month period in api format.
 * 
 * @param {ask_sdk.slot[]} report_date_slots - An array of objects representing slots from alexa request containing date params.
 * 
 * @return {number} A number representing the month period of the report being 1 for first half of hte month and 2 for second half.
 * 
 */
const __preprocess_report_month_period = (report_date_slots) => {

    let prepared_month_period = '';

    try{
        if (is_slot_value_empty(report_date_slots, 'report_month_period')) {
            prepared_month_period = parseInt(new Date().getDate()/16) + 1;
        } else {
            prepared_month_period = get_slot_id(report_date_slots, 'report_month_period');
        }

        error_helper.throw_new_custom_error_if_condition('date_invalid_month_period', 
                                                         {'prepared_month_period': prepared_month_period});

    }
    catch(err){ throw err; }


    return parseInt(prepared_month_period);
}


/**
 * An internal method to preprocess the date params the user gives us inside the alexa intent request and check if the month is January.
 * 
 * @param {ask_sdk.slot[]} report_date_slots - An array of objects representing slots from alexa request containing date params.
 * 
 * @return {boolean} A boolean indicating if the month in slots is the first of the year.
 * 
 */
const __is_first_month_of_year = (report_date_slots) => {

    let is_first_month_of_year = 
        !is_slot_value_empty(report_date_slots, 'report_year')
        && !is_slot_value_empty(report_date_slots, 'report_month_period')
        && get_slot_id(report_date_slots, 'report_month_period') == 1;

    return is_first_month_of_year;
}

/**
 * An internal method to preprocess the date params the user gives us inside the alexa intent request and check if the month is December.
 * 
 * @param {ask_sdk.slot[]} report_date_slots - An array of objects representing slots from alexa request containing date params.
 * 
 * @return {boolean} A boolean indicating if the month in slots is the last of the year.
 * 
 */
const __is_last_month_of_year = (report_date_slots) => {

    let is_last_month_of_year = 
        !is_slot_value_empty(report_date_slots, 'report_year')
        && !is_slot_value_empty(report_date_slots, 'report_month_period')
        && get_slot_id(report_date_slots, 'report_month_period') != 1

    return is_last_month_of_year;
}
