module.exports = class Report { 

    constructor(idUserdriv, customerId, report_date, report_items) {
        this.user_api_id = idUserdriv;
        this.client_api_id = customerId;
        this.prepared_date = report_date;
        this.items = report_items;
    }

};