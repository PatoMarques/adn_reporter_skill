module.exports = class ReportOnDemand { 

    constructor(on_demand_data,
        {
            group_by = {}, 
            listened_comments = [],
            requested_group = '',
            rest_size = 0,
            suggestions = ['', ''],
            is_last_on_demand = false,
            requested_items = [],
            single_comment = false,
            repeat = false,
            rest = false
        } = {}
    ) {
        this.on_demand_data = on_demand_data;
        this.group_by = group_by;
        this.listened_comments = listened_comments;
        this.requested_group = requested_group;
        this.rest_size = rest_size;
        this.suggestions = suggestions;
        this.is_last_on_demand = is_last_on_demand;
        this.requested_items = requested_items;
        this.single_comment = single_comment;
        this.repeat = repeat;
        this.rest = rest;
    }

    get_optional_parameters() {
        return {
            group_by: this.group_by,
            listened_comments: this.listened_comments,
            requested_group: this.requested_group,
            rest_size: this.rest_size,
            suggestions: this.suggestions,
            is_last_on_demand: this.is_last_on_demand,
            requested_items: this.requested_items,
            single_comment: this.single_comment,
            repeat: this.repeat,
            rest: this.rest
        }
    }

};