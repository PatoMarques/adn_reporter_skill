module.exports = class ReportDate { 

    constructor(report_year, report_month, report_month_period) {
        this.year = report_year;
        this.month = report_month;
        this.month_period = report_month_period;
    }

};