module.exports = class ReportItem { 

    constructor(idRule, ruleType, com, geoLat, geoLong) {
        this.rule_id = idRule;
        this.rule_type = ruleType;
        this.comment = com;
        this.coords_lat = geoLat;
        this.coords_long = geoLong;
    }

};