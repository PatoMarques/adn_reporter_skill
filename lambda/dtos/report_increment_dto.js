module.exports = class ReportIncrement { 

    constructor(increment_data,
        {
            increment_index = 0, 
            updated_index = 0,
            repeat = false, 
            is_last_increment = false,
            rest_size = 0,
            is_rest_of_report = false
        } = {}
    ) {
        this.increment_data = increment_data;
        this.increment_index = increment_index;
        this.updated_index = updated_index;
        this.repeat = repeat;
        this.is_last_increment = is_last_increment;
        this.rest_size = rest_size;
        this.is_rest_of_report = is_rest_of_report;
    }

    get_optional_parameters() {
        return {
            increment_data: this.increment_data,
            increment_index: this.increment_index,
            updated_index: this.updated_index,
            repeat: this.repeat,
            is_last_increment: this.is_last_increment,
            rest_size: this.rest_size,
            is_rest_of_report: this.is_rest_of_report
        }
    }

};