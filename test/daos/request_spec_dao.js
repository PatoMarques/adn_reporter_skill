const request_spec_dto = require('../dtos/request_spec_dto');

const request_samples_path = '../request_samples/';


const build_request_spec_dto = (request_spec_params) => {

    request_spec_params.request_to_test =
        get_request_data(request_spec_params.request_path);

    return new request_spec_dto(request_spec_params);

}

const get_request_data = (request_relative_path) => {
    return  require(request_samples_path + request_relative_path);
}

module.exports = { build_request_spec_dto };