module.exports = class RequestSpecDto { 

    constructor(request_spec_params) {
        this.request_to_test = request_spec_params.request_to_test;
        this.response_speech = request_spec_params.response_speech;
        this.should_end_session = request_spec_params.should_end_session;
        this.test_name = request_spec_params.specific_test_name;
        this.session_attributes = request_spec_params.session_attributes || {};
    }

};