const check_report_length = (report, { size=0 } = {}) => {
    report = JSON.parse(report);
    result = report.items.length == size;
    console.log(`check report length: ${result}`);
    return result;
}

const check_report_user = (report, {user_id = 0} = {}) => {
    report = JSON.parse(report);
    result = report.user_api_id == user_id;
    console.log(`check report user: ${result}`);
    return result;
}

const check_report_date = (report, {year = 0, month = 0, month_period = 0} = {}) => {
    report = JSON.parse(report);
    date = report.prepared_date;
    result = date.year == year && date.month == month && date.month_period == month_period;
    console.log(`check report date: ${result}`);
    return result;
}

module.exports = { check_report_date, check_report_length, check_report_user };