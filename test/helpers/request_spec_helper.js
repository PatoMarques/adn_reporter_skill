const alexaTest = require('alexa-skill-test-framework');
const { build_request_spec_dto } = require('../daos/request_spec_dao');

alexaTest.initialize(
    require('../../lambda/index.js'),
    "amzn1.ask.skill.09dba604-4938-40e1-8303-948bf3410353",
    'amzn1.ask.account.VOID'
);


const run_single_test = (request_spec_params) => {

    let request_spec_dto = build_request_spec_dto(request_spec_params);

    //console.log(request_spec_dto);
        
    execute_test(request_spec_dto);

}

const run_tests = (test_name, multi_request_params) => {

    describe(test_name, function () {
        multi_request_params.forEach(request_params => {
            run_single_test(request_params);
        });
    });

}

const check_session_attribute_facts = (attribute, checkers) => {
    let result = true;
    
    checkers.forEach(checker => {
        result = result && checker['check'](attribute, checker['values']);
    });

    console.log(`check attribute: ${result}`);

    return result;
}

module.exports = { run_single_test, run_tests, check_session_attribute_facts };


const execute_test = (request_spec_dto) => {

    describe(request_spec_dto.test_name, function () {
        alexaTest.test([
            {
                request: request_spec_dto.request_to_test,
                withSessionAttributes: request_spec_dto.request_to_test.session.attributes,
                says: request_spec_dto.response_speech,
                repromptsNothing: true,
                shouldEndSession: request_spec_dto.should_end_session,
                hasAttributes: request_spec_dto.session_attributes
            }
        ]);
    });

}