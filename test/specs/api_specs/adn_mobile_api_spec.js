const chai = require('chai');
const expect = chai.expect;
chai.use(require('chai-as-promised'))
const { call_api, call_api_through_https, get_json_response_from_api } = require ('../../../lambda/managers/api_manager');


describe('ADN Mobile API TESTS', () => {
  it('GET ...recomendaciones_1_5_2019.json ... Test connectivity.', async () =>{
    const url = 'https://156.35.171.150/recomendaciones/datos/recomendaciones_1_5_2019.json';

    await expect(call_api(url)).to.be.rejected;
  });

  it('GET ...recomendaciones_1_5_2019.json ... Test ssl invalid certificate ignoring and response being retrieved in json format.', async () =>{
    const url = 'https://156.35.171.150/recomendaciones/datos/recomendaciones_1_5_2019.json';

    raw_response = await call_api_through_https(url);
    expect(raw_response.status).to.be.equal(200);

    await expect(raw_response.json()).to.not.be.rejected;
  });

  it('GET ...recomendaciones_1_5_2019.json ... Test json response to be an array of reports with correct properties.', async () =>{
    const url = 'https://156.35.171.150/recomendaciones/datos/recomendaciones_1_5_2019.json';

    const reports = await get_json_response_from_api(url);
    expect(reports).to.be.an('Array');

    for(let report_comment of reports){
        expect(report_comment).to.be.an('Object');
        expect(report_comment.idUserdriv).to.be.a('Number');
        expect(report_comment.customerId).to.be.a('String');
        expect(report_comment.startDate).to.be.a('String');
        expect(report_comment.endDate).to.be.a('String');
        expect(report_comment.idRule).to.be.a('Number');
        expect(report_comment.ruleType).to.be.a('String');
        expect(report_comment.com).to.be.a('String');
        expect(report_comment.geoLat).to.be.a('Number');
        expect(report_comment.idRule).to.be.a('Number');
    }
  });
  
})