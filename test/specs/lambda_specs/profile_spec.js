'use strict'

const request_spec_helper = require('../../helpers/request_spec_helper');


let multi_request_params = [];
const multi_request_common_path = 'profile_requests/'
const test_name = 'Profile Intent tests';

multi_request_params.push({
    request_path: multi_request_common_path + 'set_api_id_request_1.json',
    response_speech: ``,
    should_end_session: true,
    specific_test_name: 'Set Api Id Intent test 1 - id = 5'
});

multi_request_params.push({
    request_path: multi_request_common_path + 'get_api_id_request_1.json',
    response_speech: ``,
    should_end_session: true,
    specific_test_name: 'Get Api Id Intent test 1'
});

request_spec_helper.run_tests(test_name, multi_request_params);