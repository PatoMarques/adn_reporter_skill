'use strict'

const request_spec_helper = require('../../helpers/request_spec_helper');


let multi_request_params = [];
const multi_request_common_path = 'full_report_requests/'
const test_name = 'Full Report Intent tests';

multi_request_params.push({
    request_path: multi_request_common_path + 'full_report_request_1.json',
    response_speech: `Este es tu informe completo.  Aún puedes mejorar mucho. Ánimo, con esfuerzo puedes conseguirlo. Aún puedes mejorar mucho. Trata de alcanzar la media. ¡Vamos! En el pasado has conseguido mejores resultados. Recupera tu nivel. Aún puedes mejorar mucho. Intenta acelerar más progresivamente desde 0 y recuerda utilizar la inercia de tu vehículo una vez alcanzada la velocidad máxima permitida. Aprovecha la inercia un poco antes, y si necesitas frenar, procura que sea progresivamente. Intenta usar un poco más la inercia en cuanto alcances la velocidad máxima permitida. Aumentar un poco la distancia de seguridad te ayudará. Cuando pasas por la zona de la imagen, aceleras de forma demasiado brusca. Prueba a ser más suave. En la zona de la imagen, una vez que alcanzas la velocidad máxima, intenta aprovechar la inercia del vehículo. Cuando pases por la zona de la imagen, podrías empezar la inercia un poco más tarde. Cuando pasas por la zona de la imagen, no sueles aprovechar la inercia de tu vehículo. En la zona de la imagen sueles iniciar la inercia ligeramente tarde. Intenta soltar el acelerador un poco antes Has vuelto a empeorar, pero si te lo propones puedes alcanzar la media. En relación con la lluvia aceleras mucho mejor de lo habitual, incluso mejoras a tus compañeros. Procura hacerlo igual en seco. En relación con la lluvia, cuando alcanzas la velocidad máxima sigues pisando el acelerador aún más a menudo. Procura aprovechar mucho más la inercia de tu vehículo. En relación con la lluvia tiendes a enlazar aún más el uso del acelerador y el freno. Trata de ser previsor y anticipar tus detenciones, dejando ir el vehículo y aprovechando su inercia. En relación con la lluvia te aproximas mejor en situaciones en que tienes que bajar la velocidad sin llegar a detenerte, aunque puedes mejorar respecto a tus compañeros. Trata de aprovechar mejor la inercia y decelerar progresivamente desde más lejos. Aplícalo también cuando no llueva. En relación con la lluvia lo haces mejor a la hora de hacer aproximaciones en inercia que cuando está seco, incluso mejor que tus compañeros. Sigue así y trata de hacerlo igual cuando el piso esté seco. En relación con la lluvia realizas deceleraciones más progresivas que cuando el piso está seco, aunque puedes mejorar con respecto tus compañeros. Procura seguir esa progresión tanto en mojado como en seco.`,
    should_end_session: true,
    specific_test_name: 'FullReportIntent test 1'
});

multi_request_params.push({
    request_path: multi_request_common_path + 'full_report_request_2.json',
    response_speech: `Sorry, I had trouble doing what you asked. Please try again.`,
    should_end_session: true,
    specific_test_name: 'FullReportIntent test 2'
});

request_spec_helper.run_tests(test_name, multi_request_params);