'use strict'

const request_spec_helper = require('../../helpers/request_spec_helper');


let multi_request_params = [];
const multi_request_common_path = 'help_command_requests/'
const test_name = 'Help Command tests';

multi_request_params.push({
    request_path: multi_request_common_path + 'specific_help_command_full_report_request_1.json',
    response_speech: `En este punto de la ayuda, te intentaré aclarar como tienes que indicarme que te recite el informe completo de golpe. En este modo de lectura tienes que decirme que te lea el informe completo. Por ejemplo, puedes decirme. Alexa, di a reportera ADN que me recite todo el informe del tirón.   ¿Quieres que te ayude con otra cosa?`,
    should_end_session: false,
    specific_test_name: 'Specific Help Command Intent for full report topic test'
});

multi_request_params.push({
    request_path: multi_request_common_path + 'specific_help_command_full_report_synonims_request.json',
    response_speech: `En este punto de la ayuda, te intentaré aclarar como tienes que indicarme que te recite el informe completo de golpe. En este modo de lectura tienes que decirme que te lea el informe completo. Por ejemplo, puedes decirme. Alexa, di a reportera ADN que me recite todo el informe del tirón.   ¿Quieres que te ayude con otra cosa?`,
    should_end_session: false,
    specific_test_name: 'Specific Help Command Intent for full report topic using a synonim test'
});

multi_request_params.push({
    request_path: multi_request_common_path + 'specific_help_command_without_help_topic_request.json',
    response_speech: `Esta skill sirve para acceder, mediante comandos de voz, a los informes de conducción de la empresa ADN Mobile. Pídeme un informe en concreto indicándome su fecha y te recitaré todos los comentarios de tu evaluación. Por ejemplo, puedes decirme. Alexa, di a reportera ADN que me recite el informe de Abril de 2019. Si quieres ayuda más específica, pregúntame sobre el aspecto que quieres que te aclare.  ¿Quieres que te ayude con otra cosa?`,
    should_end_session: false,
    specific_test_name: 'Specific Help Command Intent without help topic value test'
});

multi_request_params.push({
    request_path: multi_request_common_path + 'specific_help_command_help_command_topic_request.json',
    response_speech: `En este punto de la ayuda, te intentaré aclarar como puedes seleccionar un informe indicándome su fecha. Para seleccionar un informe por su fecha, me tienes que indicar su año, mes y quincena. Por ejemplo, puedes decirme. Alexa, di a reportera ADN que me recite el informe de la primera quincena de mayo de 2019.   ¿Quieres que te ayude con otra cosa?`,
    should_end_session: false,
    specific_test_name: 'Specific Help Command Intent for help command topic test'
});

multi_request_params.push({
    request_path: multi_request_common_path + 'amazon_help_intent_request.json',
    response_speech: `Esta skill sirve para acceder, mediante comandos de voz, a los informes de conducción de la empresa ADN Mobile. Pídeme un informe en concreto indicándome su fecha y te recitaré todos los comentarios de tu evaluación. Por ejemplo, puedes decirme. Alexa, di a reportera ADN que me recite el informe de Abril de 2019. Si quieres ayuda más específica, pregúntame sobre el aspecto que quieres que te aclare.  ¿Quieres que te ayude con otra cosa?`,
    should_end_session: false,
    specific_test_name: 'AMAZON.HelpIntent test'
});

request_spec_helper.run_tests(test_name, multi_request_params);