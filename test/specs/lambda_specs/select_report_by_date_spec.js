'use strict'

const { run_tests, check_session_attribute_facts } = require('../../helpers/request_spec_helper');
const { check_report_date, check_report_length, check_report_user } = require('../../helpers/report_helper');


let multi_request_params = [];
const multi_request_common_path = 'select_report_by_date_requests/'
const test_name = 'Select Report by Date tests';

multi_request_params.push({
    request_path: multi_request_common_path + 'select_report_by_date_request_1.json',
    response_speech: `Pues ya tengo tu informe correspondiente a la primera quincena de enero del año 2019 en mis manos.  Este informe en concreto consta de 13 comentarios. Uff, parece un informe bastante largo, ¿Cómo quieres que te lo cuente? Te lo puedo recitar todo de golpe, o poco a poco, tu dirás.`,
    should_end_session: false,
    specific_test_name: 'Select by Date Intent test user 5 date 1-1-2019 - first report of a year',
    session_attributes: { 
        requested_report: value => ( 
            check_session_attribute_facts(
                value,
                [
                    {check: check_report_length, values: {size:13}},
                    {check: check_report_user, values: {user_id:5}},
                    {check: check_report_date, values: {year:2019, month:1, month_period:1}}
                ]
            )
         )
    }
});

multi_request_params.push({
    request_path: multi_request_common_path + 'select_report_by_date_request_2.json',
    response_speech: `Pues ya tengo tu informe correspondiente a la primera quincena de mayo del año 2019 en mis manos.  Este informe en concreto consta de 18 comentarios. Uff, parece un informe bastante largo, ¿Cómo quieres que te lo cuente? Te lo puedo recitar todo de golpe, o poco a poco, tu dirás.`,
    should_end_session: false,
    specific_test_name: 'Select by Date Intent test user 5 date 1-5-2019 - report given month, month_period and year',
    session_attributes: { 
        requested_report: value => ( 
            check_session_attribute_facts(
                value,
                [
                    {check: check_report_length, values: {size:18}},
                    {check: check_report_user, values: {user_id:5}},
                    {check: check_report_date, values: {year:2019, month:5, month_period:1}}
                ]
            )
         )
    }
});

multi_request_params.push({
    request_path: multi_request_common_path + 'select_report_by_date_request_3.json',
    response_speech: `Lo siento, parece que se ha producido un error en la comunicación con el servicio de la empresa ADN Mobile Solutions. Es posible que se haya solicitado un informe que no existe, prueba a seleccionar un informe diferente.`,
    should_end_session: true,
    specific_test_name: 'Select by Date Intent test user 5 date 1-4-2020 - error api no report yet'
});

multi_request_params.push({
    request_path: multi_request_common_path + 'select_report_by_date_request_4.json',
    response_speech: `Lo siento, parece que se ha producido un error en la comunicación con el servicio de la empresa ADN Mobile Solutions. Es posible que se haya solicitado un informe que no existe, prueba a seleccionar un informe diferente.`,
    should_end_session: true,
    specific_test_name: 'Select by Date Intent test user 5 date 13-4-2020 - request without date params.',
});

multi_request_params.push({
    request_path: multi_request_common_path + 'select_report_by_date_request_5.json',
    response_speech: `Pues ya tengo tu informe correspondiente a la primera quincena de enero del año 2017 en mis manos.  Este informe en concreto consta de 12 comentarios. Uff, parece un informe bastante largo, ¿Cómo quieres que te lo cuente? Te lo puedo recitar todo de golpe, o poco a poco, tu dirás.`,
    should_end_session: false,
    specific_test_name: 'Select by Date Intent test user 5 date 1-1-2017 - old report with capitalized fields',
    session_attributes: { 
        requested_report: value => ( 
            check_session_attribute_facts(
                value,
                [
                    {check: check_report_length, values: {size:12}},
                    {check: check_report_user, values: {user_id:5}},
                    {check: check_report_date, values: {year:2017, month:1, month_period:1}}
                ]
            )
         )
    }
});

multi_request_params.push({
    request_path: multi_request_common_path + 'select_report_by_date_request_6.json',
    response_speech: `Lo siento, pero no soy adivina, no puedo evaluarte para una fecha futura, prueba con una fecha anterior a la actual.`,
    should_end_session: true,
    specific_test_name: 'Select by Date Intent test user 4 date 1-1-2021 - error api no report'
});

multi_request_params.push({
    request_path: multi_request_common_path + 'select_report_by_date_request_7.json',
    response_speech: `Lo siento, parece que se ha producido un error en la comunicación con el servicio de la empresa ADN Mobile Solutions. Es posible que se haya solicitado un informe que no existe, prueba a seleccionar un informe diferente.`,
    should_end_session: true,
    specific_test_name: 'Select by Date Intent test user 1 date 2-12-2019 - error api no report yet'
});

run_tests(test_name, multi_request_params);